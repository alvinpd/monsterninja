/**
 * mnadmin.js
 */

function doListItemTask( slug, task )
{
	document.adminForm.task.value = task;
	document.adminForm.slug.value = slug;

	if (typeof document.adminForm.onsubmit == "function") {
		document.adminForm.onsubmit();
	}

	document.adminForm.submit();
}

function doSortItems( sort, order )
{
	document.adminForm.sort.value = sort;
	document.adminForm.order.value = order;

	if (typeof document.adminForm.onsubmit == "function") {
		document.adminForm.onsubmit();
	}

	document.adminForm.submit();
}

/* End of file */
