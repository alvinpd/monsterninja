

-- ORM Authentication Module Database tables

CREATE TABLE IF NOT EXISTS `mn_roles` (
	`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` varchar(32) NOT NULL,
	`description` varchar(255) NOT NULL,

	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mn_roles` (`id`, `name`, `description`) VALUES(1, 'login', 'Login privileges, granted after account confirmation');
INSERT INTO `mn_roles` (`id`, `name`, `description`) VALUES(2, 'admin', 'Administrative user, has access to everything.');


CREATE TABLE IF NOT EXISTS `mn_roles_users` (
	`user_id` int(10) UNSIGNED NOT NULL,
	`role_id` int(10) UNSIGNED NOT NULL,

	PRIMARY KEY  (`user_id`,`role_id`),
	KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `mn_users` (
	`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` varchar(127) NOT NULL,
	`username` varchar(32) NOT NULL DEFAULT '',
	`password` char(50) NOT NULL,
	`logins` int(10) UNSIGNED NOT NULL DEFAULT '0',
	`last_login` int(10) UNSIGNED,
	`facebook_enabled` BOOLEAN NOT NULL DEFAULT 0,
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',

	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_username` (`username`),
	UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `mn_user_tokens` (
	`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` int(11) UNSIGNED NOT NULL,
	`user_agent` varchar(40) NOT NULL,
	`token` varchar(32) NOT NULL,
	`created` int(10) UNSIGNED NOT NULL,
	`expires` int(10) UNSIGNED NOT NULL,

	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_token` (`token`),
	KEY `fk_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `mn_roles_users`
	ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mn_users` (`id`) ON DELETE CASCADE,
	ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `mn_roles` (`id`) ON DELETE CASCADE;

ALTER TABLE `mn_user_tokens`
	ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mn_users` (`id`) ON DELETE CASCADE;



-- User Profile information
DROP TABLE IF EXISTS `mn_profiles`;
CREATE TABLE IF NOT EXISTS `mn_profiles` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id`     INT(11) UNSIGNED NOT NULL, -- The user this profile is associated to

  -- Basic user information
  `first_name`  VARCHAR(255) NOT NULL DEFAULT '',
  `last_name`   VARCHAR(255) NOT NULL DEFAULT '',
  `gender`      TINYINT(1) NOT NULL DEFAULT 0, -- 0: Male, 1: Female
  `birthday`    DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',

  -- Other information

  -- Change history
  `created`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',

  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_user_id` (`user_id`) -- A user can only have one profile
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



-- Site settings table
DROP TABLE IF EXISTS `mn_settings`;
CREATE TABLE IF NOT EXISTS `mn_settings` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(255) NOT NULL,
  `value`       TEXT NOT NULL,
  `description` TEXT NOT NULL,

  -- Change History
  `created`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by`  INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `modified`    DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` INT(11) UNSIGNED NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `mn_settings` (`name`,`value`, `description`,`created`,`created_by`) VALUES
  ('title', 'KohanaPHP Test', 'This is the title of the website.', NOW(), 1),
  ('meta_description', 'KohanaPHP', 'This is the description of the website', NOW(), 1),
  ('meta_keywords', 'KohanaPHP', 'This is the keywords related to the contents of the website', NOW(), 1);



-- Updates history table

DROP TABLE IF EXISTS `mn_updates`;
CREATE TABLE IF NOT EXISTS `mn_updates` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,

  `generated`   DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', -- Date the catalog was created
  `total`       INT(11) NOT NULL DEFAULT 0, -- Total number of games in the catalog

  `sync`        DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', -- Date the catalog was synchronized with the database
  `new`         INT(11) UNSIGNED DEFAULT 0, -- Total number of new games
  `updated`     INT(11) UNSIGNED DEFAULT 0, -- Total number of updated games

  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



-- Create the games table
DROP TABLE IF EXISTS `mn_games`;
CREATE TABLE IF NOT EXISTS `mn_games` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,

  -- Mochi media game catalog information
  `name`                VARCHAR(255) NOT NULL,
  `author`              VARCHAR(255) NOT NULL DEFAULT '',
  `author_link`         TEXT NOT NULL,
  `description`         TEXT NOT NULL,
  `category`            VARCHAR(255) NOT NULL DEFAULT '',
  `slug`                VARCHAR(255) NOT NULL,
  `uuid`                VARCHAR(36) NOT NULL,
  `game_tag`            VARCHAR(16) NOT NULL,
  `width`               INT(11) UNSIGNED NOT NULL DEFAULT 1,
  `height`              INT(11) UNSIGNED NOT NULL DEFAULT 1,
  `resolution`          VARCHAR(255) NOT NULL DEFAULT '1x1',
  `metascore`           DECIMAL(6, 3) NOT NULL DEFAULT 1.0,
  `thumbnail_url`       TEXT DEFAULT NULL,
  `thumbnail_large_url` TEXT DEFAULT NULL,
  `screen1_url`         TEXT DEFAULT NULL,
  `screen2_url`         TEXT DEFAULT NULL,
  `screen3_url`         TEXT DEFAULT NULL,
  `screen4_url`         TEXT DEFAULT NULL,
  `screen1_thumb`       TEXT DEFAULT NULL,
  `screen2_thumb`       TEXT DEFAULT NULL,
  `screen3_thumb`       TEXT DEFAULT NULL,
  `screen4_thumb`       TEXT DEFAULT NULL,
  `video_url`           TEXT DEFAULT NULL,
  `controls`            TEXT DEFAULT NULL,
  `control_scheme`      TEXT DEFAULT NULL,
  `instructions`        TEXT DEFAULT NULL,
  `key_mappings`        TEXT DEFAULT NULL,
  `tags`                TEXT DEFAULT NULL,
  `rating`              VARCHAR(255) NOT NULL DEFAULT '',
  `game_url`            TEXT DEFAULT NULL,
  `swf_url`             TEXT NOT NULL,
  `categories`          VARCHAR(255) NOT NULL DEFAULT '',
  `created`             DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `leaderboards`        BOOL DEFAULT NULL,
  `coins_enabled`       BOOL DEFAULT NULL,
  `coins_revshare_enabled` BOOL DEFAULT NULL,
  `feed_approval_created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `zip_url`             TEXT DEFAULT NULL,
  `updated`             DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `popularity`          VARCHAR(255) DEFAULT NULL,
  `alternate_url`       VARCHAR(255) DEFAULT NULL,
  `recommendation`      INTEGER NOT NULL DEFAULT 0,
  `swf_file_size`       INTEGER NOT NULL DEFAULT 0,
  `recommended`         BOOL NOT NULL DEFAULT false,
  `leaderboard_enabled` BOOL NOT NULL DEFAULT false,

  -- Site specific settings
  `local`               BOOL NOT NULL DEFAULT false,
  `published`           BOOL NOT NULL DEFAULT false,
  `featured`            BOOL NOT NULL DEFAULT false,

  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_slug` (`slug`),
  UNIQUE KEY `uniq_uuid` (`uuid`),
  UNIQUE KEY `uniq_game_tag` (`game_tag`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;



-- Leaderboard information table
DROP TABLE IF EXISTS `mn_leaderboards`;
CREATE TABLE IF NOT EXISTS `mn_leaderboards` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,

  `gameID`      VARCHAR(16)  NOT NULL DEFAULT '', -- Unique ID of the game the leaderboard belongs to.
  `boardID`     VARCHAR(32)  NOT NULL DEFAULT '', -- Unique ID of the leaderboard.

  `title`       VARCHAR(255) NOT NULL DEFAULT '', -- Title of the leaderboard
  `description` VARCHAR(255) NOT NULL DEFAULT '', -- Text description of the leaderboard (optionally supplied)
  `datatype`    VARCHAR(255) NOT NULL DEFAULT '', -- Value indicating the type of score. Values are either 'number' or 'time'. Note: time is supplied in milliseconds
  `sortOrder`   VARCHAR(255) NOT NULL DEFAULT '', -- Value indicating the sort direction of the scores. Values are either 'asc' or 'desc'.
  `scoreLabel`  VARCHAR(255) NOT NULL DEFAULT '', -- Label to indicate what the score represents. (ex: 'Track time', 'Gems', or 'Points')

  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_boardID` (`boardID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



-- Achievements information table
DROP TABLE IF EXISTS `mn_achievements`;
CREATE TABLE IF NOT EXISTS `mn_achievements` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,

	`gameID`      VARCHAR(16)  NOT NULL DEFAULT '', -- Unique ID of the game the leaderboard belongs to.
  `boardID`     VARCHAR(255) NOT NULL DEFAULT '',

  `submission`  VARCHAR(255) NOT NULL DEFAULT '', -- "score" will be passed here. This is used to filter score submissions.
  `userID`      VARCHAR(255) NOT NULL DEFAULT '', -- Unique ID of the logged-in player. This will be what you supplied in the embed parameters.
  `name`        VARCHAR(255) NOT NULL DEFAULT '', -- Username the user input for the score.
                                                  -- Plain text name of the medal achieved.
  `username`    VARCHAR(255) NOT NULL DEFAULT '', -- Username you supplied in the embed parameters.
  `sessionID`   VARCHAR(255) NOT NULL DEFAULT '', -- Returned ID provided through the embed parameters to identify the unique user playing the game.
  `score`       INT(11) NOT NULL DEFAULT 0,       -- Integer indicating the score the player is submitting

  `plays`       INT(11) UNSIGNED NOT NULL DEFAULT 0,

  -- Medal information
  `medal_bronze` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `medal_silver` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  `medal_gold`   INT(11) UNSIGNED NOT NULL DEFAULT 0,

  `thumbnail_bronze` TEXT NOT NULL,               -- Fully qualified URL to a 64x64 pixel image of the medal.
  `thumbnail_silver` TEXT NOT NULL,               -- Fully qualified URL to a 64x64 pixel image of the medal.
  `thumbnail_gold`   TEXT NOT NULL,               -- Fully qualified URL to a 64x64 pixel image of the medal.

  -- Change history
  `created`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',

  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



-- Bookmarks information table
DROP TABLE IF EXISTS `mn_bookmarks`;
CREATE TABLE IF NOT EXISTS `mn_bookmarks` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,        -- Primary key used by the table

  `user_id`     INT(11) UNSIGNED NOT NULL DEFAULT 0,             -- The user that created this bookmark
  `game_id`     INT(11) UNSIGNED NOT NULL DEFAULT 0,             -- The game the user bookmarked
  `enabled`     BOOL NOT NULL DEFAULT true,                      -- Flag for enabling and disabling the bookmark

  -- Changed history
  `created`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', -- Date and time the record was created
  `updated`     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', -- Date and time the record was updated

  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- End of file
