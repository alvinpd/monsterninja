<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file leaderboard.php
 * @brief This file defines the Leaderboard model class. It references the `mn_leaderboards` table.
 *
 * @package Monsterninja
 * @category Model
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Model_Leaderboard extends ORM
{
  protected $_rules = array(
    'gameID' => array(
      'not_empty' => null,
      'exact_length' => array(16),
    ),
    'submission' => array(
      'not_empty' => null,
    ),
    'userID' => array(
      'not_empty' => null,
    ),
    'sessionID' => array(
      'not_empty' => null,
    )
  );

  public function validate_board( &$array )
  {
    $array = Validate::factory($array)
              ->rules('gameID', $this->_rules['gameID'])
              ->rules('submission', $this->_rules['submission'])
              ->rules('userID', $this->_rules['userID'])
              ->rules('sessionID', $this->_rules['sessionID'])

              // Apply trim filter
              ->filter('gameID', 'trim')
              ->filter('boardID', 'trim')
              ->filter('title', 'trim')
              ->filter('description', 'trim')
              ->filter('datatype', 'trim')
              ->filter('sortOrder', 'trim')
              ->filter('scoreLabel', 'trim')
              ->filter('submission', 'trim')
              ->filter('userID', 'trim')
              ->filter('name', 'trim')
              ->filter('username', 'trim')
              ->filter('sessionID', 'trim')
              ->filter('score', 'trim')
              ->filter('thumbnail', 'trim')
            ;

    return $array;
  }
};

// End of file
