<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file game.php
 * @brief This file defines the Game model class. It references the `mn_games` table.
 *
 * @package Monsterninja
 * @category Model
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Model_Game extends ORM
{
	// Automatically update when the model is created or updated

	// created is the column used for storing the creation date
  protected $_created_column = array('column' => 'created', 'format' => 'Y-m-d H:i:s');

	// updated is the column used for storing the modified date
	protected $_updated_column = array('column' => 'updated', 'format' => 'Y-m-d H:i:s');
}

// End of file
