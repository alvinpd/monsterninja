<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file setting.php
 * @brief This file defines the Setting model class. It references the `mn_settings` table.
 *
 * @package Monsterninja
 * @category Model
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Model_Setting extends ORM
{
	// Automatically update when the model is updated and created
	protected $_created_column = array('column' => 'created', 'format' => 'Y-m-d H:i:s');
	protected $_updated_column = array('column' => 'modified', 'format' => 'Y-m-d H:i:s');

	// Create rules
	protected $_rules = array (
		'name' => array (
			'not_empty' => null,
			'min_length' => array(5),
			'max_length' => array(32),
		),
		'value' => array(
			'not_empty' => null,
			'min_length' => array(3),
			'max_length' => array(255),
		),
		'description' => array(
			'not_empty' => null,
			'min_length' => array(5),
			'max_length' => array(255),
		),
	);

	// Callbacks for validation
	protected $_callbacks = array(
		'name' => 'name_available',
	);

	public function validate_save( &$array )
	{
		$array = Validate::factory($array)
						->rules('name', $this->_rules['name'])
						->rules('value', $this->_rules['value'])
						->filter('name', 'trim')
						->filter('description', 'trim')
						->filter('value', 'trim')
						->callback('name', array($this, $this->_callbacks['name']));

		return $array;
	}

	/**
	 * Validate the contents of the edit $_POST variable.
	 *
	 * @param array $array
	 * @return
	 */
	public function validate_edit( &$array )
	{
		$array = Validate::factory($array)
						->rules('name', $this->_rules['name'])
						->rules('value', $this->_rules['value'])
						->rules('description', $this->_rules['description'])
						->filter('name', 'trim')
						->filter('value', 'trim')
						->filter('description', 'trim')
						->filter('created_alias', 'trim');

		return $array;
	}

	public function validate_update( &$array )
	{
		$array = Validate::factory($array)
						->rules('name', $this->_rules['name'])
						->rules('value', $this->_rules['value'])
						->filter('name', 'trim')
						->filter('description', 'trim')
						->filter('value', 'trim');

		return $array;
	}

	public function name_available(Validate $array, $field)
	{
		if ($this->unique_name_exists($array[$field])) {
			$array->error($field, 'name_available', array($array[$field]));
		}
	}

	/**
	 * Tests if a unique key value exists in the database
	 *
	 * @param   mixed        value  the value to test
	 * @return  boolean
	 */
	public function unique_name_exists($value)
	{
		return (bool) DB::select(array('COUNT("*")', 'total_count'))
						->from($this->_table_name)
						->where('name', '=', $value)
						->execute($this->_db)
						->get('total_count');
	}
}
