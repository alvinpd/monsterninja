<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file bookmark.php
 * @brief This file defines the Bookmark model class. It references the `mn_bookmarks` table.
 *
 * @package Monsterninja
 * @category Model
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Model_Bookmark extends ORM
{
	protected $_rules = array(
		'user_id' => array(
			'not_empty'	=> null, # This field is required
			'numeric'   => null, # This field must be a number
		),
		'game_id' => array(
			'not_empty'	=> null, # This field is required
			'numeric'   => null, # This field must be a number
		),
		'enabled' => array(
			'not_empty'	=> null, # This field is required
			'numeric'   => null, # This field must be a number
		),
	);

	// Automatically update when the model is created or updated
	// created is the column used for storing the creation date
  protected $_created_column = array('column' => 'created', 'format' => 'Y-m-d H:i:s');

	// updated is the column used for storing the modified date
	protected $_updated_column = array('column' => 'updated', 'format' => 'Y-m-d H:i:s');

	/**
	 * Validate if the $_POST variable validates against the required data.
	 *
	 * @param array $post This is the $_POST variable to be validated.
	 */
	public function validate_bookmark( &$post )
	{
		$post = Validate::factory($post)
							->rules('user_id', $this->_rules['user_id'])
							->rules('game_id', $this->_rules['game_id'])
							->rules('enabled', $this->_rules['enabled']);

		return $post;
	}
}

// End of file
