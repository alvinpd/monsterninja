<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file game.php
 * @brief This file defines the Update model class. It references the `mn_updates` table.
 *
 * @package Monsterninja
 * @category Model
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Model_Update extends ORM
{
  
}

// End of file
