<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file ajax.php
 * @brief Ajax request handler for the Monster Ninja application.
 *
 * @package Monsterninja
 * @category Controller
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Controller_Ajax extends Controller_Base
{
  public function before()
  {
    parent::before();

    $this->auto_render = false;
  }

  public function action_score()
	{
		// This must be an ajax request
		if ( !Request::$is_ajax )
		{
			return;
		}

		// A user must be already logged in to bookmark games
		if ( !Auth::instance()->logged_in() )
		{
			return;
		}

    $leaderboard = ORM::factory('leaderboard');
    $post = $leaderboard->validate_board($_POST);

    if ( $post->check() ) {
      // Add the new leadearboard in the leaderboards table if it does not exist yet.
      $ld = ORM::factory('leaderboard')
            ->where('boardID','=',$post['boardID'])
            ->and_where('gameID','=',$post['gameID'])
            ->find();

			// Save the new leaderboard data if it does not exist
      if ( $ld->id == 0 AND $post['submission'] == 'score' ) {
        $ld->values($post);
        $ld->save();
      }

			// Save the score data for this game
			$score = null;
			if ( $post['submission'] == 'score' ) {
				$score = ORM::factory('achievement')
								->where('boardID','=',$post['boardID'])
								->and_where('userID','=',$post['userID'])->find();
			}
			elseif ( $post['submission'] == 'medal' ) {
				$score = ORM::factory('achievement')
								->where('gameID','=',$post['gameID'])
								->and_where('userID','=',$post['userID'])->find();
			}

			if ( $score == null ) {
				return;
			}

			// Determine if a record is already available in the database
			if ( $score->id == 0 ) {
				$score->plays = 0;
				$score->thumbnail_bronze = '';
				$score->thumbnail_gold = '';
				$score->thumbnail_silver = '';

				$score->values($post);
			}

			// Save the score submission for this game and user
      if ( $post['submission'] == 'score' ) {
				$score->plays++;

				if ( $post['sortOrder'] == 'asc' ) {
					// The lower score the better
					if ( $post['score'] < $score->score ) {
						$score->score = $post['score'];
					}
				}
				elseif ( $post['sortOrder'] == 'desc' ) {
					// The higher score the better
					if ( $post['score'] > $score->score ) {
						$score->score = $post['score'];
					}
				}
      }
			// Save the medal submission for this game and user
      elseif ( $post['submission'] == 'medal' ) {
				if ( stristr($post['name'], 'gold') != FALSE ) {
					$score->medal_gold ++;
					$score->thumbnail_gold = $post['thumbnail'];
				}
				elseif ( stristr($post['name'], 'silver') != FALSE ) {
					$score->medal_silver ++;
					$score->thumbnail_silver = $post['thumbnail'];
				}
				elseif ( stristr($post['name'], 'bronze') != FALSE ) {
					$score->medal_bronze ++;
					$score->thumbnail_bronze = $post['thumbnail'];
				}
      }
      else {
        /* Nothing to do. */
      }

			// Save achievement of the user
			$score->save();
    }
  }

	public function action_bookmark()
	{
		// This must be an ajax request
		if ( !Request::$is_ajax )
		{
			return;
		}

		// A user must be already logged in to bookmark games
		if ( !Auth::instance()->logged_in() )
		{
			return;
		}


	}
};

// End of file
