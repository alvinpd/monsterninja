<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file base.php
 * @brief This file defines the base controller class for this web application.
 *
 * @package Monsterninja
 * @category Controller
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Controller_Base extends Controller_Template
{
	// Template page
	public $template = 'layouts/base';

	// User authentication
	public $user;
	public $auth_required = FALSE; // Set to true if all actions of a controller requires authentication
	public $auth_role = array('login'); // Roles allowed to access authenticated actions
	public $secure_actions = array(); // Only specific actions of a controller requires authentication

	/**
	 * The before() method is called before your controller action. In our template controller we
	 * override this method so that we can set up default values. These variables are then available
	 * to our controllers if they need to be modified.
	 */
	public function before()
	{
		parent::before();

		// Open session
		$this->session = Session::instance();

		// Initialize the Facebook API
		$fb = $this->session->get('facebook');
		if ( $fb == null ) {
			$fb = new Facebook(array(
				'appId' => ORM::factory('setting')->where('name','=','fb_application_id')->find()->value,
				'secret' => ORM::factory('setting')->where('name','=','fb_application_secret')->find()->value,
				'cookie' => true,
			));

			$this->session->set('facebook', $fb);
		}

		// Check user authentication and role
		$request = Request::instance();
		$action_name = $request->action;
		$directory_name = Request::instance()->directory;
		if ( ($this->auth_required == TRUE || in_array($action_name, $this->secure_actions)) AND
				 Auth::instance()->logged_in($this->auth_role) == FALSE ) {
			// Check if the user is already logged in
			if ( Auth::instance()->logged_in() ) {
				Request::instance()->redirect(Route::get('user')->uri(array('controller' => 'profile')));
			} else {
				if ( $directory_name == 'admin' ) {
					Request::instance()->redirect(Route::get('admin')->uri(array('action' => 'login')));
				}
				else {
					Request::instance()->redirect(Route::get('default')->uri());
				}
			}
		}

		// Determine if the required page is to be rendered and set required variables
		if ($this->auto_render) {
			// Initialize empty values
			$this->template->title   = ORM::factory('setting')->where('name', '=', 'title')->find()->value;
			$this->template->content = '';

			$this->template->styles = array();
			$this->template->scripts = array();

		}
	}

	/**
	 * The after() method is called after your controller action. In our template controller we
	 * override this method so that we can make any last minute modifications to the template before
	 * anything is rendered.
	 */
	public function after()
	{
		if ($this->auto_render) {
			$styles = array(
				'media/stylesheets/screen.css' => 'screen, projection',
			);

			$scripts = array(
				'http://xs.mochiads.com/static/pub/swf/leaderboard.js',
				'media/javascripts/jquery-1.4.2.min.js',
				'media/javascripts/mn.js',
			);

			$this->template->styles = array_merge( $this->template->styles, $styles );
			$this->template->scripts = array_merge( $scripts, $this->template->scripts );
		}

		parent::after();
	}
}
