<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file main.php
 * @brief This is the user profile management file. This file implements the user
 *        controller profile class.
 *
 * @package Monsterninja
 * @category Controller/User
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Controller_User_Profile extends Controller_User_Main
{
  public $auth_required = true;

	/**
	 * Display the main profile page of the user.
	 *
	 * @author Alvin Difuntorum <alvinpd09@gmail.com>
	 */
  public function action_index()
  {
    $facebook = $this->session->get('facebook');
    $fb_session = $facebook->getSession();

    $uid = null;
    if ( isset($fb_session) AND $fb_session != null ) {
      try {
      $uid = $facebook->getUser();
      } catch ( FacebookApiException $e) {
        error_log($e);
      }
    }

		// Generate the user profile page from the template page and bind data to it
    $this->template->content = View::factory('user/profile/index')
            ->bind('uid', $uid)
						->bind('gold', $medals_gold)
						->bind('silver', $medals_silver)
						->bind('bronze', $medals_bronze)

						->bind('top_games', $top_games)
						->bind('activities', $activities);

		// Set the title of the page
		$this->template->title = __('Profile').' - '.$this->template->title;

		// Retrieve the medals achieved by the user
		$medals_gold = DB::select(array('SUM("medal_gold")', 'gold'))
						->from('achievements')->where('userID', '=', $uid)
						->execute()->get('gold');
		$medals_silver = DB::select(array('SUM("medal_silver")', 'silver'))
						->from('achievements')->where('userID', '=', $uid)
						->execute()->get('silver');
		$medals_bronze = DB::select(array('SUM("medal_bronze")', 'bronze'))
						->from('achievements')->where('userID', '=', $uid)
						->execute()->get('bronze');

		// Generate the 10 top games played by this user
		$query = 'SELECT `g`.`name` AS `name`,`g`.`slug` AS `slug`,`g`.`thumbnail_url` AS `thumbnail`,SUM(`a`.`plays`) AS `plays` '.
				'FROM `mn_games` AS `g` INNER JOIN `mn_achievements` AS `a` ON `g`.`game_tag`=`a`.`gameID` '.
				"WHERE `a`.`userID`='".$uid."' ".
				'GROUP BY `a`.`gameID` ORDER BY `plays` DESC LIMIT 10';

		$top_games = DB::query(Database::SELECT, $query)->execute();

		// Get friends using the Graph API
		$friends = $facebook->api('me/friends');
		$items = '';
		foreach ($friends['data'] as $friend) {
			$items = $items.'\''.$friend['id'].'\',';
		}

		// Select the latest friend activities of the logged in user
		$query = 'SELECT `a`.`score` AS `score`,`a`.`userID` AS `uid`,`l`.`title`,`g`.`name` AS `name`, `g`.`slug` AS `slug` '.
						 'FROM `mn_achievements` AS `a` '.
						 'INNER JOIN `mn_leaderboards` AS `l`  ON `l`.`boardID` = `a`.`boardID` '.
						 'INNER JOIN `mn_games` AS `g` ON `g`.`game_tag` = `a`.`gameID` '.
						 'WHERE `a`.`userID` IN('.substr($items,0,-1).') ORDER BY `a`.`updated` DESC LIMIT 20';

		$activities = DB::query(Database::SELECT, $query)->execute();
	}

  public function action_bookmark()
  {
    $this->template->content = View::factory('user/profile/bookmark');
  }
};

// End of file
