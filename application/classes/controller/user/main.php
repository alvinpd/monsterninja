<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file main.php
 * @brief This is the main user management file. This file implements the user
 *        controller main class.
 *
 * @package Monsterninja
 * @category Controller/User
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Controller_User_Main extends Controller_Base
{
  // Enable secure actions
  //public $secure_actions = array('logout');

  /**
   * This is the callback action after a successful authentication from Facebook.
   */
  public function action_index()
  {
    // Check if a user is already logged in
    if ( Auth::instance()->logged_in() ) {
      Request::instance()->redirect(Route::get('user')->uri(array('controller' => 'profile')));
    }

    // Get the Facebook API instance from the session class
    $facebook = $this->session->get('facebook');

    // Get the Facebook API session
    $session = $facebook->getSession();

    // Check if a valid session is available and redirect if it is not available
    if ( $session == null ) {
      Request::instance()->redirect(Route::get('default')->uri());
    }

    // Get the logged in Facebook user from the session
    $uid = $facebook->getUser();
    $user = ORM::factory('user')->where('username','=',$uid)->find();

    // Check if the user already exists. If it does not, then create a new one.
    if ( $user->id == 0 ) {
      $post = array(
        'username' => $uid,
        'password' => $uid,
        'password_confirm' => $uid,
        'email' => $uid.'@monsterninja.com',
      );

      // Load the validation rules, filters, etc...
      $post = $user->validate_create($post);

      // Check iff the post data validates using the roles setup in the user model
      if ( $post->check() ) {
        // Affects the sanitized vars to the user object
        $user->values($post);
        $user->facebook_enabled = 1;

        // Create the account
        $user->save();

        // Add the login role to the user
        $login_role = new Model_Role(array('name' => 'login'));
        $user->add('roles', $login_role);

        // Sign in the user
        Auth::instance()->login($uid, $uid);
      }
    } else {
      $post = array(
        'username' => $uid,
        'password' => $uid,
      );

      // Instantiate a new user
      $user = ORM::factory('user');

      // Check auth status
      $status = $user->login($post, false);

      // If the user failed to login, redirect to the log out page.
      if ( !$status ) {
        Request::instance()->redirect(Route::get('user')->uri(array('action' => 'logout')));
      }
    }

    // Save the facebook session value
    $facebook->setSession($session, true);

		$next = $this->session->get('next', Route::get('user')->uri(array('controller' => 'profile')));
		$this->session->delete('next');

    // Redirect the user to the profile page
    Request::instance()->redirect($next);
  }

  /**
   * Log outs the currently logged in user from the website.
   */
  public function action_logout()
  {
    // Get the Facebook API instance from the session class
    $facebook = $this->session->get('facebook');
    $logout_url = $facebook->getLogoutUrl(array(
			'next' => URL::base(true,true).Route::get('default')->uri(),
    ));

    // Sign out the user
    Auth::instance()->logout();

    // Delete the facebook session
    $facebook->setSession(null);

    // Proceed with the logout
    Request::instance()->redirect($logout_url);
  }

  /**
   * Log ins the user to the website by authenticating through Facebook.
   */
  public function action_login()
  {
    if ( Auth::instance()->logged_in() ) {
      // Check if a user is already logged in
      Request::instance()->redirect(Route::get('user')->uri(array('controller' => 'profile')));
    }

		// Create the link to the next page to redirect to after successful login in facebook
		$this->session->delete('next');
		$next = Arr::get($_GET, 'next') == null ? Route::get('user')->uri(array('action' => 'index')) : Arr::get($_GET, 'next');
		$this->session->set('next', URL::base(true,true).$next);

    // Redirect to the facebook log in page
    $facebook = $this->session->get('facebook');
    $login_url = $facebook->getLoginUrl(array(
              'next' => URL::base(true,true).Route::get('user')->uri(array('action' => 'index')),
              'cancel_url' => URL::base(true,true).Route::get('default')->uri(),
            ));

    Request::instance()->redirect($login_url);
  }
}

// End of file
