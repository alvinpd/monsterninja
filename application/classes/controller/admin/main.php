<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file main.php
 *
 * @package Monsterninja
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010. Monster Ninja Games
 */
class Controller_Admin_Main extends Controller_Base
{
	public $template = 'layouts/admin';

	public $auth_role      = array('admin');
	public $secure_actions = array('index', 'logout');

	/**
	 * This function will overload the __construct() function of the parent class to support https
	 * protocol access for the admin pages.
	 *
	 * @param Kohana_Request $request request handle
	 */
//	public function __construct(Kohana_Request $request)
//	{
//		parent::__construct($request);
//
//		// Enable https access
//		if ( Request::$protocol !== 'https' ) {
//			Request::instance()->redirect(URL::site(Request::instance()->uri(), 'https'));
//		}
//	}

	public function before()
	{
		parent::before();

		$this->template->title = __('Website Administration');
	}

	public function after()
	{
		if ($this->auto_render) {
			$scripts = array(
				'media/javascripts/jquery-ui-1.8.1.custom.min.js',
				'media/javascripts/mnadmin.js',
			);

			$this->template->scripts = array_merge( $this->template->scripts, $scripts );
		}

		parent::after();
	}

	public function action_index()
	{
		$this->template->content = View::factory('admin/main/index')
						->bind('item_total', $item_total)
						->bind('item_published', $item_published)
						->bind('item_premium', $item_premium)
						->bind('item_recommended', $item_recommended)
						->bind('item_local', $item_local)
						->bind('item_local_size', $item_local_size)

						->bind('user_total', $user_total);

		$games = ORM::factory('game');

		$item_total = $games->count_all();
		$item_published = $games->where('published','=',1)->count_all();
		$item_premium = $games->where('coins_enabled','=',1)->count_all();
		$item_recommended = $games->where('coins_enabled','=',0)
						->and_where('recommended','=',1)->and_where('recommendation','>=',3)
						->and_where('leaderboard_enabled','=',1)->count_all();
		$item_local = $games->where('local','=',1)->count_all();
		$item_local_size = DB::select(array('SUM("swf_file_size")','size'))
						->from('games')->where('local','=',1)
						->execute()->get('size');

		$user = ORM::factory('user');
		$user_total = $user->count_all();
	}

	public function action_login()
	{
		$username = '';
		$this->template->content = View::factory('admin/main/login')->bind('username', $username);

		if ( $_POST ) {
			// Instantiate a new user
			$user = ORM::factory('user');

			if ( $_POST['action'] == 'login' ) {
				// Check auth
				$status = $user->login($_POST);

				// If the post data validates using the rules setup in the user model
				if ( $status ) {
					// Redirect to the dashboard
					Request::instance()->redirect(Route::get('admin')->uri());
				}
				else {
					// Get errors for display in view
					$this->template->content->errors = $_POST->errors('signin');
					$username = $_POST['username'];
				}
			}
		}
	}

	public function action_logout()
	{
		// Sign out the user
		Auth::instance()->logout();

		// Redirect the user to the login page
		Request::instance()->redirect(Route::get('admin')->uri(array('action' => 'login')));
	}

	public function action_register()
	{
		if ($_POST)
		{
			# Instantiate a new user
			$user = ORM::factory('user');

			# Load the validation rules, filters, etc...
			$post = $user->validate_create($_POST);

			# If the user validates using the rules setp in the user model
			if ($post->check())
			{
				# Affects the sanitized vars to the user objct
				$user->values($post);

				# Create the account
				$user->save();

				#Add the admin role to the user
				$admin_role = new Model_Role(array('name' => 'admin'));
				$user->add('roles', $admin_role);

				# Sign in the user
				Auth::instance()->login($post['username'], $post['password']);

				# Redirect the user to the admin index page
				Request::instance()->redirect(Route::get('admin')->uri());
			}
		}
		else
		{
			$this->template->content = View::factory('admin/main/register');
		}
	}
}

// End of file
