<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file setting.php
 *
 * @package Monsterninja
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010. Monster Ninja Games
 */

class Controller_Admin_Setting extends Controller_Admin_Main
{
	public $auth_required = TRUE;
	
	public function action_index()
	{
		$this->template->title = __('Settings - ').$this->template->title;
		$this->template->content = View::factory('admin/setting/index')
						->bind('settings', $settings);

		$settings = ORM::factory('setting')->find_all();
	}

	/**
	 * Views a setting record from the database.
	 *
	 * @param integer $id The id of the item to be viewed.
	 */
	public function action_view($id = 0)
	{
		$error = null;

		$this->template->title = __('View Setting - ').$this->template->title;
		$setting = ORM::factory('setting', $id);

		if ( $setting->loaded() ) {
			$this->template->content = View::factory('admin/setting/view')->bind('setting', $setting);
		}
		else {
			$this->template->content = View::factory('admin/setting/index')
							->bind('error', $error)
							->bind('settings', $settings);

			$error = __('Unable to find the setting you have requested. Please check the value you have provided.');

			$settings = ORM::factory('setting')->find_all();
		}
	}

	/**
	 * Edits a setting record from the database.
	 *
	 * @param integer $id The id of the item to be edited.
	 */
	public function action_edit($id = 0)
	{
		$setting = ORM::factory('setting', $id);

		if ( !$setting->loaded() ) {
			Request::instance()->redirect(Route::get('admin')->uri());
		}

		$this->template->content = View::factory('admin/setting/edit')->bind('setting', $setting);
	}

	public function action_new()
	{
		$setting = ORM::factory('setting', 0);
		$this->template->content = View::factory('admin/setting/edit')->bind('setting', $setting);
	}

	/**
	 * Save the modified or new setting item to the database.
	 */
	public function action_save()
	{
		$setting = ORM::factory('setting', isset($_POST['id']) ? trim($_POST['id']) : 0 );
		if ( $setting->loaded() ) {
			$post = $setting->validate_update($_POST);
		} else {
			$post = $setting->validate_save($_POST);
		}

		if ($post->check()) {
			$setting->values($post);

			if ( $setting->id == 0 ) {
				$setting->created_by = Auth::instance()->get_user();
				$setting->modified_by = 0;
			}
			else {
				$setting->modified_by = Auth::instance()->get_user();
			}

			$setting->save();

			$this->template->content = View::factory('admin/setting/view')->bind('setting', $setting);
		}
		else {
			$setting->values($post);
			$this->template->content = View::factory('admin/setting/edit')->bind('setting', $setting);
		}
	}

	/**
	 * Deletes a setting record from the database.
	 *
	 * @param integer $id The id of the item to be deleted.
	 */
	public function action_delete($id = 0)
	{
		// Retrieve the record from the database
		$setting = ORM::factory('setting', $id);

		// Create the content view for this action
		$this->template->content = View::factory('admin/setting/delete');
	}
}

// End of file
