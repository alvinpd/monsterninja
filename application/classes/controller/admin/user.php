<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file user.php
 *
 * @package Monsterninja
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010. Monster Ninja Games
 */

class Controller_Admin_User extends Controller_Admin_Main
{
	public $auth_required = TRUE;
	
	public function after()
	{
		if ( $this->auto_render ) {
			$this->template->content->bind('menu', $menu);

			$menu = View::factory('admin/user/_menu');
		}

		parent::after();
	}

	public function action_index()
	{
		$this->template->content = View::factory('admin/user/index')
						->bind('user_count', $user_count)

						->bind('latest_users', $latest_users)
						->bind('active_users', $active_users)
						->bind('latest_logins', $latest_logins);

		$user = ORM::factory('user');

		$user_count = $user->count_all();

		$latest_users = $user->order_by('created', 'desc')->limit(20)->find_all();
		$active_users = $user->order_by('logins', 'desc')->limit(20)->find_all();
		$latest_logins = $user->order_by('last_login', 'desc')->limit(20)->find_all();
	}

	public function action_list()
	{
		$this->template->content = View::factory('admin/user/list')
						->bind('users', $users);

		$users = ORM::factory('user')->find_all();
	}

	public function action_view($id = 0)
	{
		$this->template->content = View::factory('admin/user/view')
						->bind('user', $user)

						->bind('total_plays', $total_plays)
						->bind('total_gold', $total_gold)
						->bind('total_silver', $total_silver)
						->bind('total_bronze', $total_bronze)

						->bind('activities', $activities)
						->bind('games', $games);

		$user = ORM::factory('user', $id);
		$total_plays = DB::select(array('SUM("plays")', 'total'))
						->from('achievements')->where('userID', '=', $user->username)
						->execute()->get('total');
		$total_gold = DB::select(array('SUM("medal_gold")', 'total'))
						->from('achievements')->where('userID', '=', $user->username)
						->execute()->get('total');
		$total_silver = DB::select(array('SUM("medal_silver")', 'total'))
						->from('achievements')->where('userID', '=', $user->username)
						->execute()->get('total');
		$total_bronze = DB::select(array('SUM("medal_bronze")', 'total'))
						->from('achievements')->where('userID', '=', $user->username)
						->execute()->get('total');

		// Get the recent activities of the user
		$query = 'SELECT `g`.`name` AS `name`,`g`.`thumbnail_url` AS `thumbnail`,`g`.`slug` AS `slug`, '.
						 '       `l`.`title` AS `title`, `l`.`datatype` AS `type`,`a`.`score` AS `score` '.
						 'FROM `mn_games` AS `g` '.
						 'INNER JOIN `mn_achievements` AS `a` ON `a`.`gameID` = `g`.`game_tag` '.
						 'INNER JOIN `mn_users` AS `u` ON `a`.`userID` = `u`.`username` '.
						 'INNER JOIN `mn_leaderboards` AS `l` ON `l`.`boardID` = `a`.`boardID` '.
						 'WHERE `a`.`userID` = \''.$user->username.'\' ORDER BY `a`.`updated` DESC LIMIT 20';

		$activities = DB::query(Database::SELECT, $query)->execute();

		// Get the latest games played by the user
		$query = 'SELECT `g`.`name` AS `name`,`g`.`thumbnail_url` AS `thumbnail`,`g`.`slug` AS `slug`, '.
						 '`a`.`plays` AS `plays` '.
						 'FROM `mn_games` AS `g` '.
						 'INNER JOIN `mn_achievements` AS `a` ON `a`.`gameID` = `g`.`game_tag` '.
						 'INNER JOIN `mn_users` AS `u` ON `a`.`userID` = `u`.`username` '.
						 'WHERE `a`.`userID` = \''.$user->username.'\' ORDER BY `a`.`plays` DESC LIMIT 20';
		$games = DB::query(Database::SELECT, $query)->execute();
	}
}

// End of file
