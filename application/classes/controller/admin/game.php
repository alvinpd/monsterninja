<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file game.php
 *
 * @package Monsterninja
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010. Monster Ninja Games
 */

class Controller_Admin_Game extends Controller_Admin_Main
{
	public $auth_required = TRUE;

	public function after()
	{
		if ( $this->auto_render ) {
			$this->template->content->bind('menu', $menu);

			$menu = View::factory('admin/game/_menu');
		}

		parent::after();
	}

	public function action_index()
	{
		$this->template->title = __('Contents : ').$this->template->title;
		$this->template->content = View::factory('admin/game/index')
						->bind('total', $total)

						->bind('premium_count', $premium_count)
						->bind('premium_published', $premium_published)
						->bind('premium_local', $premium_local)
						->bind('premium_local_size', $premium_local_size)

						->bind('recommended_count', $recommended_count)
						->bind('recommended_published', $recommended_published)

						->bind('latest_premium', $latest_premium)
						->bind('latest_recommended', $latest_recommended)

						->bind('top_games', $top_games)
						->bind('updates', $updates);

		$game = ORM::factory('game');
		$total = $game->count_all();

		$premium_count = $game->where('coins_enabled','=',1)->count_all();
		$premium_published = $game->where('coins_enabled','=',1)->and_where('published','=',1)->count_all();
		$premium_local = $game->where('coins_enabled','=',1)->and_where('local','=',1)->count_all();
		$premium_local_size = DB::select(array('SUM("swf_file_size")','size'))
						->from('games')->where('local','=',1)
						->execute()->get('size');

		$recommended_count = $game->where('coins_enabled','=',0)->and_where('recommended','=',1)
						->and_where('recommendation','>=',3)->and_where('leaderboard_enabled','=',1)->count_all();
		$recommended_published = $game->where('coins_enabled','=',0)->and_where('recommended','=',1)
						->and_where('recommendation','>=',3)->and_where('leaderboard_enabled','=',1)
						->and_where('published','=',1)->count_all();

		$latest_premium = $game->where('coins_enabled','=',1)->order_by('updated','desc')->limit(30)
						->find_all();
		$latest_recommended = $game->where('coins_enabled','=',0)->and_where('recommended','=',1)
						->and_where('recommendation','>=',3)->and_where('leaderboard_enabled','=',1)
						->limit(30)->find_all();

		// Get the top played games
		$query = 'SELECT `g`.`name` AS `name`,`g`.`thumbnail_url` AS `thumbnail`,`g`.`slug` AS `slug`,SUM(`a`.`plays`) AS `plays` '.
						 'FROM `mn_games` AS `g` INNER JOIN `mn_achievements` AS `a` ON `g`.`game_tag` = `a`.`gameID` '.
						 'GROUP BY `a`.`gameID` ORDER BY `plays` DESC LIMIT 30';

		$top_games = DB::query(Database::SELECT, $query)->execute();

		// Get the update history for the games
		$updates = ORM::factory('update')->limit(20)->find_all();
	}

	public function action_premium()
	{
		$this->template->title = __('Premium Items : ').$this->template->title;
		$this->template->content = View::factory('admin/game/premium')
						->bind('limit', $limit)
						->bind('search', $search)
						->bind('offset', $offset)
						->bind('pages', $pages)

						->bind('sort', $sort)
						->bind('order', $order)

						->bind('total', $total)
						->bind('games', $games);

		$this->_do_task();

		$limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
		$search = isset($_POST['search']) ? trim($_POST['search']) : '';
		$offset = isset($_POST['offset']) ? intval(trim($_POST['offset'])) : 0;

		$sort = isset($_POST['sort']) ? trim($_POST['sort']) : 'name';
		$order = isset($_POST['order']) ? trim($_POST['order']) : 'asc';

		$result = DB::select(array('COUNT("*")','total'))->from('games')
						->where('coins_enabled', '=', 1)
						->and_where('name', 'like', '%'.$search.'%');

		$games = ORM::factory('game')->where('coins_enabled', '=', 1)
						->and_where('name', 'like', '%'.$search.'%');

		$total = $result->execute()->get('total');
		$page_count = $total / intval($limit);
		$page_count = ($total % $limit) == 0 ? intval($page_count) : intval($page_count) + 1;

		if ( ($offset > $page_count) AND ($page_count > 0) ) {
			$offset = $page_count - 1;
		}

		$games = $games->limit($limit)->offset($offset * $limit)
							->order_by($sort,$order)->find_all();
		$pages = array();
    for ( $i = 0; $i < $page_count; $i++ ) {
      $pages = array_merge($pages, array($i => 'page '.($i+1).' of '.$page_count));
    }
	}

	public function action_recommended()
	{
		$this->template->title = __('Recommended Items : ').$this->template->title;
		$this->template->content = View::factory('admin/game/recommended')
						->bind('limit', $limit)
						->bind('search', $search)
						->bind('offset', $offset)
						->bind('pages', $pages)

						->bind('sort', $sort)
						->bind('order', $order)

						->bind('total', $total)
						->bind('games', $games);

		$this->_do_task();

		$limit = isset($_POST['limit']) ? $_POST['limit'] : 20;
		$search = isset($_POST['search']) ? trim($_POST['search']) : '';
		$offset = isset($_POST['offset']) ? intval(trim($_POST['offset'])) : 0;

		$sort = isset($_POST['sort']) ? trim($_POST['sort']) : 'name';
		$order = isset($_POST['order']) ? trim($_POST['order']) : 'asc';

		$result = DB::select(array('COUNT("*")','total'))->from('games')
						->where('coins_enabled', '=', 0)
						->and_where('recommended', '=', 1)
						->and_where('recommendation', '>=', 3)
						->and_where('leaderboard_enabled', '=', 1)
						->and_where('name', 'like', '%'.$search.'%');

		$games = ORM::factory('game')->where('coins_enabled', '=', 0)
						->and_where('recommended', '=', 1)
						->and_where('recommendation', '>=', 3)
						->and_where('leaderboard_enabled', '=', 1)
						->and_where('name', 'like', '%'.$search.'%');

		$total = $result->execute()->get('total');
		$page_count = $total / intval($limit);
		$page_count = ($total % $limit) == 0 ? intval($page_count) : intval($page_count) + 1;

		if ( ($offset > $page_count) AND ($page_count > 0) ) {
			$offset = $page_count - 1;
		}

		$games = $games->limit($limit)->offset($offset * $limit)
						->order_by($sort,$order)->find_all();
		$pages = array();
    for ( $i = 0; $i < $page_count; $i++ ) {
      $pages = array_merge($pages, array($i => 'page '.($i+1).' of '.$page_count));
    }
	}

	public function action_view($slug=null)
	{
		$error = null;
		$players = null;
		$plays = 0;

		$this->template->content = View::factory('admin/game/view')
						->bind('error', $error)
						->bind('game', $game)
						->bind('plays', $plays)
						->bind('players', $players);

		$this->_do_task();

		if ($slug == null) {
			$error = __('Please provide a valid game slug.');
		} else {
			$game = ORM::factory('game', array('slug' => $slug));

			if ( $game->id == 0 ) {
				$this->template->title = __('Error : ').$this->template->title;
				$error = __('The game for the slug ('.$slug.') you have provided does not exist.');
			} else {
				$this->template->title = $game->name.' : '.$this->template->title;

				// Determine the number of times the game was played
				$plays = DB::select(array('SUM("plays")', 'total'))
								->from('achievements')->where('gameID','=',$game->game_tag)
								->execute()->get('total');

				// Get the players of this game
				$query = 'SELECT `u`.`id` AS `id`,`u`.`username` AS `username`,`a`.`plays` AS `plays` '.
								 'FROM `mn_users` AS `u` '.
								 'INNER JOIN `mn_achievements` AS `a` ON `u`.`username` = `a`.`userID` '.
								 'WHERE `a`.`gameID` = \''.$game->game_tag.'\' ORDER BY `plays` DESC LIMIT 20';

				$players = DB::query(Database::SELECT, $query)->execute();
			}
		}
	}

	private function _do_task()
	{
		// Check and execute tasks
		$slug = isset($_POST['slug']) ? trim($_POST['slug']) : '';
		$task = isset($_POST['task']) ? trim($_POST['task']) : '';
		$game = ORM::factory('game')->where('slug', '=', $slug)->find();

		if ( $game->id != 0 ) {
			switch ($task) {
				case 'publish':     $game->published = 1; break;
				case 'unpublish':   $game->published = 0; break;
				case 'server':      $game->local = 1; break;
				case 'mochi':       $game->local = 0; break;
				case 'featured':    $game->featured = 1; break;
				case 'notfeatured': $game->featured = 0; break;
			}
			$game->save();
		}
	}
}

// End of file
