<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @file home.php
 * @brief This file defines the home controller class for this web application. The home controller
 *        handles request for the home page. This class is also the default route controller.
 *
 * @package Monsterninja
 * @category Controller
 * @author Alvin Difuntorum <alvinpd09@gmail.com>
 * @copyright (c) 2010 Monster Ninja Games. All rights reserved.
 */

class Controller_Home extends Controller_Base
{
	/**
	 * This function is inherited from the Controller_Base class.
	 */
	public function before()
	{
		parent::before();

		if ( Request::instance()->action == 'play' ) {
			// The template used by the 'play' action of this controller is the 'layouts/play' template.
			$this->template->set_filename( 'layouts/play' );
		}
	}

	/**
	 * This function is inherited from the Controller_Base class.
	 */
	public function after()
	{
		if ( $this->auto_render ) {
			$this->template->content->bind('top_players', $top_players);

			// Create the menu to be displayed in the home pages
			$this->template->menu = View::factory( '_menu' );

			// Select the top 10 players
			$top_players = DB::select(DB::expr('userID,SUM(`plays`) as plays'))
					->from('achievements')
					->group_by('userID')
					->limit(10)
					->order_by('plays','desc')
					->execute();
		}

		parent::after();
	}

	/**
	 * A test function for testing the operation of the score/medal posting functionality. This action
	 * must be disabled in the production environment.
	 */
	public function action_post()
	{
		$this->template->content = View::factory('home/post');
	}

	/**
	 * This function generates the main home page and the contents associated with it.
	 *
	 * @author Alvin Difuntorum <alvinpd09@gmail.com>
	 */
	public function action_index()
	{
		// Change the title of the page
		$this->template->title = __('Home - ').$this->template->title;

		// Generate the home main page from the template page and bind data to it
		$this->template->content = View::factory( 'home/index' )
				->bind('premium', $premium)
				->bind('recommended', $recommended)
				->bind('latest_games', $latest_games)

				->bind('top_games', $top_games)
				->bind('activities', $activities);

		// Create a game ORM object
		$game = ORM::factory('game');

		// Select the latest 6 featured premium games
		$premium = $game->where('coins_enabled', '=', 1)
				->and_where('published', '=', 1)
				->and_where('featured', '=', 1)
				->order_by('updated', 'DESC')
				->limit(6)
				->find_all();

		// Select the latest 6 featured recommended games
		$recommended = $game->where('coins_enabled', '=', 0)
				->and_where('published', '=', 1)
				->and_where('recommended', '=', 1)
				->and_where('leaderboard_enabled', '=', 1)
				->and_where('recommendation', '>=', 3)
				->order_by('updated', 'DESC')
				->limit(6)
				->find_all();

		// Select the latest 10 premium and recommended games
		$latest_games = $game->where('published', '=', 1)
				->limit(10)
				->order_by('updated','DESC')
				->find_all();

		// Select the top 10 games
		$query = 'SELECT `g`.`name` AS `name`,`g`.`slug` AS `slug`,`g`.`thumbnail_url` AS `thumbnail`,SUM(`a`.`plays`) AS `plays` '.
				'FROM `mn_games` AS `g` INNER JOIN `mn_achievements` AS `a` ON `g`.`game_tag`=`a`.`gameID` '.
				'GROUP BY `a`.`gameID` ORDER BY `plays` DESC LIMIT 10';

		$top_games = DB::query(Database::SELECT, $query)->execute();

		// Select the recent activities
		$query = 'select `g`.`name` as `name`,`g`.`slug` as `slug`,`g`.`thumbnail_url` as `thumbnail`, '.
						 '       `a`.`userID` as `uid`,`a`.`score` as `score`,`l`.`title` as `title` '.
						 'from `mn_games` as `g` '.
						 'inner join `mn_achievements` as `a` on `g`.`game_tag` = `a`.`gameID` '.
						 'inner join `mn_leaderboards` as `l` on `a`.`gameID` = `l`.`gameID` '.
						 'where `a`.`submission` = \'score\' order by `a`.`updated` desc  limit 16';

		$activities = DB::query(Database::SELECT, $query)->execute();
	}

	/**
	 * This function generates the premium games page and the contents associated with it.
	 *
	 * @author Alvin Difuntorum <alvinpd09@gmail.com>
	 */
	public function action_premium()
	{
		// Change the title of the page
		$this->template->title = __('Premium Games - ').$this->template->title;

		// Generate the premium page from the template page and bind data to it
		$this->template->content = View::factory( 'home/premium' )
				->bind('featured', $featured)
				->bind('premium', $premium)
				->bind('top_games', $top_games)
				->bind('latest_games', $latest_games);

		// Create a game ORM object
		$game = ORM::factory('game');

		// Select the latest 6 featured premium games
		$featured = $game->where('coins_enabled', '=', 1)
				->and_where('published', '=', 1)
				->and_where('featured', '=', 1)
				->order_by('updated', 'DESC')
				->limit(6)
				->find_all();

		// Select all the premium games from the database
		$premium = $game->where('coins_enabled', '=', 1)
				->and_where('published', '=', 1)
				->find_all();

		// Select the latest 10 premium games
		$latest_games = $game->where('coins_enabled', '=', 1)
				->and_where('published', '=', 1)
				->limit(10)
				->order_by('updated', 'DESC')
				->find_all();

		// Select the top 10 premium games
		$query = 'SELECT `g`.`name` AS `name`,`g`.`slug` AS `slug`,`g`.`thumbnail_url` AS `thumbnail`,SUM(`a`.`plays`) AS `plays` '.
				'FROM `mn_games` AS `g` INNER JOIN `mn_achievements` AS `a` ON `g`.`game_tag`=`a`.`gameID` '.
				'WHERE `g`.`coins_enabled`=1 '.
				'GROUP BY `a`.`gameID` ORDER BY `plays` DESC LIMIT 10';

		$top_games = DB::query(Database::SELECT, $query)->execute();
	}

	/**
	 * This function generates the recommended games page and the contents associated with it.
	 *
	 * @author Alvin Difuntorum <alvinpd09@gmail.com>
	 */
	public function action_recommended()
	{
		// Change the title of the page
		$this->template->title = __('Recommended Games - ').$this->template->title;

		// Generate the recommended page from the template page and bind data to it
		$this->template->content = View::factory( 'home/recommended' )
						->bind('featured', $featured)
						->bind('recommended', $recommended)
						->bind('top_games', $top_games)
						->bind('latest_games', $latest_games);

		// Create a game ORM object
		$game = ORM::factory('game');

		// Select the latest 6 featured recommended games
		$featured = $game->where('coins_enabled', '=', 0)
				->and_where('published', '=', 1)
				->and_where('recommended', '=', 1)
				->and_where('leaderboard_enabled', '=', 1)
				->and_where('recommendation', '>=', 3)
				->order_by('updated', 'DESC')
				->limit(6)
				->find_all();

		// Select all the recommended games from the database
		$recommended = $game->where('coins_enabled', '=', 0)
						->and_where('published', '=', 1)
						->limit(120)
						->find_all();

		// Select the latest 10 recommended games
		$latest_games = $game->where('coins_enabled', '=', 0)
				->and_where('published', '=', 1)
				->limit(10)
				->order_by('updated', 'desc')
				->find_all();

		// Select the top 10 recommended games
		$query = 'SELECT `g`.`name` AS `name`,`g`.`slug` AS `slug`,`g`.`thumbnail_url` AS `thumbnail`,SUM(`a`.`plays`) AS `plays` '.
				'FROM `mn_games` AS `g` INNER JOIN `mn_achievements` AS `a` ON `g`.`game_tag`=`a`.`gameID` '.
				'WHERE `g`.`coins_enabled`=0 '.
				'GROUP BY `a`.`gameID` ORDER BY `plays` DESC LIMIT 10';

		$top_games = DB::query(Database::SELECT, $query)->execute();
	}

	public function action_bookmark()
	{
		// Change the title of the page
		$this->template->title = __('Bookmarked Games - ').$this->template->title;

		$this->template->content = View::factory('home/bookmark');
	}

	/**
	 * Get and show the game information given the game slug.
	 *
	 * @param string $slug
	 *
	 * @author Alvin Difuntorum <alvinpd09@gmail.com>
	 */
	public function action_show( $slug = null )
	{
		if ( $slug == null ) {
			Request::instance()->redirect(Route::get('default')->uri());
		}

		$this->template->content = View::factory( 'home/view' )
						->bind('game', $game);
		$game = ORM::factory('game')->where('slug', '=', $slug)
						->find();

		if ( $game->id == 0 ) {
			$this->template->content->errors = array(__('The requested game was not found.'));
		}
	}

	/**
	 * Play a game selected by the user. The slug of the game to be played is required by this
	 * function.
	 *
	 * @param string $slug The slug of the game to be played.
	 *
	 * @author Alvin Difuntorum <alvinpd09@gmail.com>
	 */
	public function action_play( $slug = null )
	{
		$back = isset(Request::$referrer) ? Request::$referrer : URL::base(true,true).Route::get('default')->uri();

		if ( $slug == null ) {
			Request::instance()->redirect(Route::get('default')->uri());
		}

		$facebook = $this->session->get('facebook');
		$fb_session = $facebook->getSession();

		$uid = null;
		$achievement = null;
		$leaderboard = null;
		if ( isset($fb_session) AND $fb_session != null )
		{
			try {
				$uid = $facebook->getUser();
			} catch ( FacebookApiException $e) {
				error_log($e);
			}
		}

		$this->template->content = View::factory( 'home/play' )
						->bind('game', $game)

						->bind('back', $back)
						->bind('session', $fb_session)
						->bind('uid', $uid)
						->bind('achievement', $achievement)

						->bind('leaderboard', $leaderboard);

		$game = ORM::factory('game')->where('slug', '=', $slug)
						->find();

		if ( $game->id == 0 ) {
			$this->template->content->errors = array(__('The requested game was not found.'));
		}
		else {
			$this->template->title = $game->name.' - '.$this->template->title;

			if ($uid != null)
			{
				$achievement = ORM::factory('achievement')
					->where('userID','=',$uid)
					->and_where('gameID','=',$game->game_tag)
					->find();

				if ($achievement->loaded()) {
					$leaderboard = ORM::factory('leaderboard')
						->where('boardID', '=', $achievement->boardID)
						->find();
				}
			}
		}
	}
}
