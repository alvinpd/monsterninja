<?php defined('SYSPATH') or die('No direct script access.'); ?>

<div id="content-menu" class="span-24 last">
	<ul>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting')),
								 'Overview', array('class' => Request::instance()->action == 'index' ? 'selected' : '')) ?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting','action' => 'new')),
								 'Add Setting', array('class' => Request::instance()->action == 'new' ? 'selected' : '')) ?>
		</li>
	</ul>
</div>

<?php if ( isset($error) AND $error ) : ?>
<div class="span-24 last">
	<div class="error">
		<?php echo $error ?>
	</div>
</div>
<?php endif ?>

<div id="content-table" class="span-24 last">
	<table>
		<tr>
			<th width="60"><?php echo __('#') ?></th>
			<th width="25%">Name</th>
			<th>Value</th>
			<th width="130"></th>
		</tr>
	<?php $i = 1 ?>
	<?php $k = 0 ?>
	<?php foreach($settings as $setting) : ?>
		<tr class="row<?php echo $k ?>">
			<td><?php echo $i++ ?></td>
			<td style="text-align: left"><?php echo $setting->name ?></td>
			<td style="text-align: left"><?php echo htmlentities($setting->value) ?></td>
			<td>
				<?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting','action'=>'view','id'=>$setting->id)),
							__('View'), array('title' => 'View '.$setting->name)) ?> |
				<?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting','action'=>'edit','id'=>$setting->id)),
							__('Edit'), array('title' => 'Edit '.$setting->name)) ?> |
				<?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting','action'=>'delete','id'=>$setting->id)),
							__('Delete'), array('title' => 'Delete '.$setting->name)) ?>
			</td>
		</tr>
		<?php $k = 1 - $k ?>
	<?php endforeach ?>
	</table>
</div>
