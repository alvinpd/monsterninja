<?php defined('SYSPATH') or die('No direct script access.') ?>
<div id="content-menu" class="span-24 last">
	<ul>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting')),
								 'More Settings...', array('class' => Request::instance()->action == 'index' ? 'selected' : '')) ?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting','action' => 'new')),
								 'Add Setting', array('class' => Request::instance()->action == 'new' ? 'selected' : '')) ?>
		</li>
	</ul>
</div>

<div class="span-24">
	<div class="notice">
		This feature is under construction. Manually delete the record from the database for now.
	</div>
</div>
