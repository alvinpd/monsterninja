<?php defined('SYSPATH') or die('No direct script access.') ?>
<div id="content-menu" class="span-24 last">
	<ul>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting')),
								 'Overview', array('class' => Request::instance()->action == 'index' ? 'selected' : '')), "\n" ?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting','action' => 'new')),
								 'Add Setting', array('class' => Request::instance()->action == 'new' ? 'selected' : '')), "\n" ?>
		</li>
	</ul>
</div>

<div class="span-4">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-gear"></div>
			<?php echo __('Details'), "\n" ?>
		</h3>
		<p><?php echo $setting->id == 0 ? __('New Setting') : __('Edit Setting') ?></p>
	<?php if ($setting->id > 0) : ?>
		<p>Created: <?php echo HTML::date($setting->created, '%b %d, %Y') ?></p>
		<p>Author: <?php echo $setting->created_by == '0' ? __('unknown') : ORM::factory('user', $setting->created_by)->username ?></p>
		<p>Updated: <?php echo $setting->modified == '0000-00-00 00:00:00' ? __('unmodified'): HTML::date($setting->modified, '%b %d, %Y') ?></p>
		<p>Author: <?php echo $setting->modified_by == '0' ? __('unknown') : ORM::factory('user', $setting->modified_by)->username ?></p>
	<?php endif ?>
	</div>

<?php if ($setting->id > 0) : ?>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-gear"></div>
			<?php echo __('Actions'), "\n" ?>
		</h3>
		<p><?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting', 'action' => 'view', 'id' => $setting)), __('View'),
									array('title' => 'View '.$setting->name)) ?></p>
		<p><?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting', 'action' => 'delete', 'id' => $setting)), __('Delete'),
									array('title' => 'Delete '.$setting->name)) ?></p>
	</div>
<?php endif ?>
</div>

<div class="span-20 last">
	<?php echo Form::open(Route::get('admin')->uri(array('controller'=>'setting','action'=>'save'))), "\n" ?>
		<div class="content-info">
			<h3>
				<div class="image ui-icon-gear"></div>
				<?php echo __('Name'), "\n" ?>
			</h3>
			<p>
				<?php echo Form::input('name', $setting->name, array('class'=>'text required')), "\n" ?>
				This item is required (not empty and must be at least 5 characters long) and must be unique.
				The maximum number of characters is 32.
			</p>
		</div>
		<div class="content-info">
			<h3>
				<div class="image ui-icon-gear"></div>
				<?php echo __('Description'), "\n" ?>
			</h3>
			<p>
				<?php echo Form::textarea('description', $setting->description), "\n" ?>
				This item is not required (can be empty). The maximum number of characters is 255.
			</p>
		</div>
		<div class="content-info">
			<h3>
				<div class="image ui-icon-gear"></div>
				<?php echo __('Value'), "\n" ?>
			</h3>
			<p>
				<?php echo Form::textarea('value', $setting->value, array('class' => 'required')), "\n" ?>
				This item is required (not empty and must be at least 5 characters long).
				The maximum number of characters is 255.
			</p>
		</div>
		<button type="submit" class="button">
			<img src="/media/stylesheets/blueprint/icons/tick.png" alt=""/>
			<?php echo $setting->id == 0 ? __('Save Setting') : __('Update Setting') ?>
		</button>
		<?php echo Form::hidden('id', $setting->id)?>
	<?php echo Form::close(), "\n" ?>
</div>
