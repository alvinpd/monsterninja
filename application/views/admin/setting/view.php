<?php defined('SYSPATH') or die('No direct script access.'); ?>
<div id="content-menu" class="span-24 last">
	<ul>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting')),
								 'More Settings...', array('class' => Request::instance()->action == 'index' ? 'selected' : '')) ?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting','action' => 'new')),
								 'Add Setting', array('class' => Request::instance()->action == 'new' ? 'selected' : '')) ?>
		</li>
	</ul>
</div>

<div class="span-4">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-gear"></div>
			<?php echo __('Details'), "\n" ?>
		</h3>
		<p><?php echo $setting->name ?></p>
		<p>Created: <?php echo HTML::date($setting->created, '%b %d, %Y') ?></p>
		<p>Author: <?php echo $setting->created_by == '0' ? __('unknown') : ORM::factory('user', $setting->created_by)->username ?></p>
		<p>Updated: <?php echo $setting->modified == '0000-00-00 00:00:00' ? __('unmodified'): HTML::date($setting->modified, '%b %d, %Y') ?></p>
		<p>Author: <?php echo $setting->modified_by == '0' ? __('unknown') : ORM::factory('user', $setting->modified_by)->username ?></p>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-gear"></div>
			<?php echo __('Actions'), "\n" ?>
		</h3>
		<p><?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting', 'action' => 'edit', 'id' => $setting)), __('Edit'),
									array('title' => 'Edit '.$setting->name)) ?></p>
		<p><?php echo HTML::anchor(Route::get('admin')->uri(array('controller'=>'setting', 'action' => 'delete', 'id' => $setting)), __('Delete'),
									array('title' => 'Delete '.$setting->name)) ?></p>
	</div>
</div>

<div class="span-20 last">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-gear"></div>
			<?php echo __('Description'), "\n"?>
		</h3>
		<p><?php echo $setting->description ?></p>
	</div>
<div class="content-info">
		<h3>
			<div class="image ui-icon-gear"></div>
			<?php echo __('Value'), "\n"?>
		</h3>
		<p><?php echo htmlentities($setting->value) ?></p>
	</div>
</div>
