<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php if (isset($errors['username']) OR isset($errors['password'])) :?>
<div class="span-24 last">
	<div class="error">
		<?php echo 'Please enter a valid username and/or password.', "\n" ?>
	</div>
</div>
<?php endif ?>

<?php if (isset($errors['signin'])) :?>
<div class="span-24 last">
	<div class="error">
		<?php echo 'An error occurred while logging in. ('.$errors['signin'].')', "\n" ?>
	</div>
</div>
<?php endif ?>

<div class="span-6">
	<div id="content-login">
		<?php echo Form::open(), "\n" ?>
			<?php echo Form::hidden('action', 'login'), "\n"; ?>
			<?php echo Form::label('Username'), "<br/>\n"; ?>
			<?php echo Form::input('username', $username, array('class'=>'text', 'style'=>'width: 100%; margin-bottom: 5px')), "<br/>\n"; ?>
			<?php echo Form::label('Password'), "<br/>\n"; ?>
			<?php echo Form::input('password', NULL, array('type' => 'password','class'=>'text', 'style'=>'width: 100%')), "<br/>\n"; ?>
			<button type="submit" class="button">
				<img src="/media/stylesheets/blueprint/icons/key.png" alt=""/> <?php echo __('Log In') ?>
			</button>
		<?php echo Form::close(), "\n" ?>
	</div>
</div>

<div class="span-18 last">
	&nbsp;
</div>
