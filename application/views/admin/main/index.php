<?php defined('SYSPATH') or die('No direct script access.'); ?>

<script type="text/javascript">
	$(function(){
		$("#accordion-index-info").accordion({ header: "h3", autoHeight: false });
		$("#accordion-index-todo").accordion({ header: "h3" });
	})
</script>

<div class="span-6">
	<div class="span-6 last">
		<div id="accordion-index-info">
			<div>
				<h3><a href="#">Games</a></h3>
				<div>
					<p><?php echo number_format($item_total) ?> total items </p>
					<p><?php echo number_format($item_published) ?> published items </p>
					<p><?php echo number_format($item_premium) ?> premium items </p>
					<p><?php echo number_format($item_recommended) ?> recommended items </p>
					<p><?php echo number_format($item_local) ?> hosted items </p>
					<p class="end"><?php echo number_format($item_local_size / (1024 * 1024), 2) ?> MB of hosted items </p>
				</div>
			</div>
			<div>
				<h3><a href="#">Users</a></h3>
				<div>
					<p class="end"><?php echo $user_total ?> total users</p>
				</div>
			</div>
			<div>
				<h3><a href="#">Settings</a></h3>
				<div>
					<p class="end">TODO: Add Details</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="span-18 last">
	<div class="span-18 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-radio-on"></div>
				Todo Items
			</h3>
			<div>
				Add to do items content here. 
			</div>
		</div>
	</div>

</div>
