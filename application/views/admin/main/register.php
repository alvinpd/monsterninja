<?php defined('SYSPATH') or die('No direct script access.'); ?>

<div class="span-6">
	<div id="content-login">
		<?php echo Form::open(), "\n" ?>
			<?php echo Form::hidden('action', 'register'), "\n"; ?>
			<?php echo Form::label('Username'), "<br/>\n"; ?>
			<?php echo Form::input('username', null, array('class'=>'text', 'style'=>'width: 100%; margin-bottom: 5px')), "<br/>\n"; ?>
			<?php echo Form::label('Email'), "<br/>\n"; ?>
			<?php echo Form::input('email', null, array('class'=>'text', 'style'=>'width: 100%; margin-bottom: 5px')), "<br/>\n"; ?>
			<?php echo Form::label('Password'), "<br/>\n"; ?>
			<?php echo Form::input('password', NULL, array('type' => 'password','class'=>'text', 'style'=>'width: 100%; margin-bottom: 5px')), "<br/>\n"; ?>
			<?php echo Form::label('Confirm Password'), "<br/>\n"; ?>
			<?php echo Form::input('password_confirm', NULL, array('type' => 'password','class'=>'text', 'style'=>'width: 100%')), "<br/>\n"; ?>
			<button type="submit" class="button">
				<img src="/media/stylesheets/blueprint/icons/tick.png" alt=""/> <?php echo __('Register User') ?>
			</button>
		<?php echo Form::close(), "\n" ?>
	</div>
</div>

<div class="span-18 last">
	&nbsp;
</div>
