<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php $action = Request::instance()->action ?>
<div id="content-menu" class="span-24 last">
	<ul>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'user')),
								 'Overview', array('class' => $action == 'index' ? 'selected' : '')) ?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'user', 'action' => 'list')),
								 'Users', array('class' => $action == 'list' ? 'selected' : ''))?>
		</li>
	</ul>
</div>

