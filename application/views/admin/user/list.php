<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php echo $menu ?>

<div id="content-table" class="span-24 last">
	<table>
		<?php
			$i = 1;
			$k = 0;
		?>
		<tr>
			<th width="60"><?php echo __('#') ?></th>
			<th><?php echo __('Username') ?></th>
			<th><?php echo __('Email') ?></th>
			<th width="100"><?php echo __('Role') ?></th>
			<th width="100"><?php echo __('Logins') ?></th>
			<th width="160"><?php echo __('Last Login') ?></th>
		</tr>
	<?php foreach($users as $user) : ?>
		<tr class="row<?php echo $k ?>">
			<td><?php echo $i++ ?></td>
			<td style="text-align: left">
			<?php if ($user->facebook_enabled) : ?>
				<a href="<?php echo URL::base().Route::get('admin')->uri(array('controller' => 'user', 'action' => 'view', 'id' => $user->id)) ?>">
					<fb:name uid="<?php echo $user->username ?>" linked="false"></fb:name>
				</a>
			<?php else : ?>
				<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'user', 'action' => 'view', 'id' => $user->id)), $user->username) ?>
			<?php endif ?>
			</td>
			<td>
			<?php if ($user->facebook_enabled) : ?>
				<span style="color: #ff0000"><?php echo __('Email is unavailable.'), "\n" ?></span>
			<?php else : ?>
				<?php echo $user->email, "\n" ?>
			<?php endif ?>
			</td>
			<td>
		<?php if ($user->facebook_enabled) : ?>
				<?php echo __('login'), "\n" ?>
		<?php else : ?>
			<?php $roles = $user->roles->find_all() ?>
			<?php foreach ( $roles as $role) : ?>
				<?php echo $role->name, "\n" ?><br/>
			<?php endforeach ?>
		<?php endif ?>
			</td>
			<td><?php echo $user->logins ?></td>
			<td><?php echo HTML::date($user->last_login, '%b %d, %Y %H:%M:%S') ?></td>
		</tr>
		<?php $k = 1 - $k ?>
	<?php endforeach ?>
	</table>
</div>
