<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php echo $menu ?>

<div class="span-4">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-person"></div>
			<fb:name uid="<?php echo $user->username?>" linked="false"></fb:name>
		</h3>
		<p style="text-align: center">
			<fb:profile-pic uid="<?php echo $user->username?>" size="small" linked="false"></fb:profile-pic>
		</p>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-person"></div>
			<?php echo __('Information'), "\n" ?>
		</h3>
		<p><?php echo $user->logins ?> logins</p>
		<p title="Date the user was added.">Added: <?php echo HTML::date($user->created, '%b %d, %Y') ?></p>
		<p title="Latest date logged in.">Latest: <?php echo HTML::date($user->last_login, '%b %d, %Y') ?></p>
	</div>
</div>

<div class="span-20 last">
	<div class="span-5">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-person"></div>
				<?php echo __('Plays') ?>
			</h3>
			<p style="text-align: center"><?php echo number_format($total_plays) ?></p>
		</div>
	</div>

	<div class="span-5">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-person"></div>
				<?php echo __('Gold Medals') ?>
			</h3>
			<p style="text-align: center"><?php echo number_format($total_gold) ?></p>
		</div>
	</div>

	<div class="span-5">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-person"></div>
				<?php echo __('Silver Medals') ?>
			</h3>
			<p style="text-align: center"><?php echo number_format($total_silver) ?></p>
		</div>
	</div>

	<div class="span-5 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-person"></div>
				<?php echo __('Bronze Medals') ?>
			</h3>
			<p style="text-align: center"><?php echo number_format($total_bronze) ?></p>
		</div>
	</div>

	<div class="span-20 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-person"></div>
				<?php echo __('Recent Activities') ?>
			</h3>
			<div style="height: 100%; margin: 0; border: 0; border: 0">
			<?php $i = 0 ?>
			<?php foreach ($activities as $activity) : ?>
				<div class="content-info1<?php echo ($i+1) % 4 == 0 ? ' end' : '' ?>" title="<?php echo $activity['title'] ?>">
					<a href="<?php echo URL::site(Route::get('admin')->uri(array('controller'=>'game','action'=>'view','id' => $activity['slug']))) ?>"
						 title="<?php echo $activity['name'] ?>">
						<?php echo HTML::image($activity['thumbnail'], array('width'=>'50','height'=>'50')) ?>
					</a>
					<h4><?php echo substr($activity['title'], 0, 15)?></h4>
					<?php echo number_format($activity['score'])?>
				</div>
				<?php $i++ ?>
			<?php endforeach ?>
			</div>
		</div>
	</div>

	<div class="span-20 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-person"></div>
				<?php echo __('Top Played Games') ?>
			</h3>
			<div style="height: 100%; margin: 0; border: 0; border: 0">
			<?php $i = 0 ?>
			<?php foreach ($games as $game) : ?>
				<div class="content-info1<?php echo ($i+1) % 4 == 0 ? ' end' : '' ?>" title="<?php echo $game['name'] ?>">
					<a href="<?php echo URL::site(Route::get('admin')->uri(array('controller'=>'game','action'=>'view','id'=>$game['slug']))) ?>"
						 title="<?php echo $game['name'] ?>">
						<?php echo HTML::image($game['thumbnail'], array('width'=>'50','height'=>'50')) ?>
					</a>
					<h4><?php echo substr($game['name'], 0, 13)?></h4>
					<?php echo $game['plays'] ?>
				</div>
				<?php $i++ ?>
			<?php endforeach ?>
			</div>
		</div>
	</div>
</div>
