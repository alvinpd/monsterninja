<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php echo $menu ?>

<script type="text/javascript">
	$(function(){
		$("#accordion-index-info").accordion({ header: "h3", autoHeight: false });
	})
</script>

<div class="span-6">
	<div class="span-6 last">
		<div id="accordion-index-info">
			<div>
				<h3><a href="#"><?php echo __('Users') ?></a></h3>
				<div>
					<p class="end"><?php echo number_format($user_count) ?> total users</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="span-18 last">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-person"></div>
			<?php echo __('Latest Registered Users') ?>
		</h3>
		<ul>
		<?php foreach($latest_users as $user) : ?>
			<li>
				<a href="<?php echo URL::base().Route::get('admin')->uri(array('controller' => 'user', 'action' => 'view', 'id' => $user->id)) ?>">
					<fb:profile-pic uid="<?php echo $user->username ?>" size="square" width="60" height="60" linked="false"></fb:profile-pic>
				</a>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-person"></div>
			<?php echo __('Active Users') ?>
		</h3>
		<ul>
		<?php foreach($active_users as $user) : ?>
			<li>
				<a href="<?php echo URL::base().Route::get('admin')->uri(array('controller' => 'user', 'action' => 'view', 'id' => $user->id)) ?>">
					<fb:profile-pic uid="<?php echo $user->username ?>" size="square" width="60" height="60" linked="false"></fb:profile-pic>
				</a>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-person"></div>
			<?php echo __('Latest Logged In Users') ?>
		</h3>
		<ul>
		<?php foreach($latest_logins as $user) : ?>
			<li>
				<a href="<?php echo URL::base().Route::get('admin')->uri(array('controller' => 'user', 'action' => 'view', 'id' => $user->id)) ?>">
					<fb:profile-pic uid="<?php echo $user->username ?>" size="square" width="60" height="60" linked="false"></fb:profile-pic>
				</a>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
</div>

