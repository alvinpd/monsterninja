<?php defined('SYSPATH') or die('No direct script access.');

	function get_sort_order($name,$sort,$order)
	{
		if ( $name == $sort ) {
			return $order == 'asc' ? 'desc' : 'asc';
		}

		return 'asc';
	}
?>

<?php echo $menu ?>

<?php echo Form::open(null, array('name' => 'adminForm')), "\n" ?>
	<div class="span-24 last" style="margin-bottom: 5px">
		<div class="span-20">
			Search Name <?php echo Form::input('search', $search) ?>
			Display
			<?php echo Form::select('limit',
						array('20' => '20', '40' => '40', '60' => '60', '80' => '80', '100' => '100'),
						$limit, array('onchange' => 'document.adminForm.submit()')) ?>
			Page
			<?php echo Form::select('offset', $pages, $offset, array('onchange' => 'document.adminForm.submit()')) ?>
		</div>
		<div class="span-4 last" style="text-align: right">
			<?php echo $total ?> records found.
		</div>
	</div>

	<div id="content-table" class="span-24 last">
		<table>
			<tr>
				<th width="60"><?php echo __('#') ?></th>
				<th>
					<a href="javascript:void(0);" title="Sort items by name"
						 onclick="return doSortItems('name','<?php echo get_sort_order('name',$sort,$order) ?>')">
						Name
				<?php if ($sort == 'name') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
				<th width="100">
					<a href="javascript:void(0);" title="Sort items by coins revenue share"
						 onclick="return doSortItems('coins_revshare_enabled','<?php echo get_sort_order('coins_revshare_enabled',$sort,$order) ?>')">
						Shared
				<?php if ($sort == 'coins_revshare_enabled') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
				<th width="120">
					<a href="javascript:void(0);" title="Sort items by leaderboard support"
						 onclick="return doSortItems('leaderboard_enabled','<?php echo get_sort_order('leaderboard_enabled',$sort,$order) ?>')">
						Leaderboard
				<?php if ($sort == 'leaderboard_enabled') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
				<th width="110">
					<a href="javascript:void(0);" title="Sort items by swf file size"
						 onclick="return doSortItems('swf_file_size','<?php echo get_sort_order('swf_file_size',$sort,$order) ?>')">
						Size
				<?php if ($sort == 'swf_file_size') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
				<th width="100">
					<a href="javascript:void(0);" title="Sort items by the swf server location"
						 onclick="return doSortItems('local','<?php echo get_sort_order('local',$sort,$order) ?>')">
						Server
				<?php if ($sort == 'local') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
				<th width="100">
					<a href="javascript:void(0);" title="Sort items by the swf server location"
						 onclick="return doSortItems('published','<?php echo get_sort_order('published',$sort,$order) ?>')">
						Published
				<?php if ($sort == 'published') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
				<th width="100">
					<a href="javascript:void(0);" title="Sort items by featured status"
						 onclick="return doSortItems('featured','<?php echo get_sort_order('featured',$sort,$order) ?>')">
						Featured
				<?php if ($sort == 'featured') { ?>
						&nbsp;<?php echo HTML::image('media/images/sort_'.$order.'.png'); ?>
				<?php } ?>
					</a>
				</th>
			</tr>
		<?php $i = ($limit * $offset) + 1 ?>
		<?php $k = 0 ?>
		<?php foreach($games as $game) { ?>
			<tr class="row<?php echo $k ?>">
				<td><?php echo $i++ ?></td>
				<td style="text-align: left">
					<?php echo HTML::anchor(
								Route::get('admin')->uri(array('controller' => 'game', 'action' => 'view', 'id' => $game->slug)),
								$game->name) ?>
				</td>
				<td><?php echo $game->coins_revshare_enabled ? 'Enabled' : 'Disabled' ?></td>
				<td><?php echo $game->leaderboard_enabled ? 'Enabled' : 'Disabled' ?></td>
				<td><?php echo number_format($game->swf_file_size/(1024), 2).' KB' ?></td>
				<td>
					<a href="javascript:void(0);" title="Set server to <?php echo $game->local ? 'Mochi' : 'Server'?>"
						 onclick="return doListItemTask('<?php echo $game->slug ?>', '<?php echo $game->local ? 'mochi' : 'server'?>')">
						<?php echo $game->local ? 'Server' : 'Mochi' ?>
					</a>
				</td>
				<td>
					<a href="javascript:void(0);" title="<?php echo $game->published ? 'Unpublish' : 'Publish'?>"
						 onclick="return doListItemTask('<?php echo $game->slug ?>', '<?php echo $game->published ? 'unpublish' : 'publish'?>')">
						<?php echo $game->published ? 'Yes' : 'No' ?>
					</a>
				</td>
				<td>
					<a href="javascript:void(0);" title="<?php echo $game->featured ? 'Unset featured' : 'Set featured'?>"
						 onclick="return doListItemTask('<?php echo $game->slug ?>','<?php echo $game->featured ? 'notfeatured' : 'featured'?>')">
						<?php echo $game->featured ? 'Yes' : 'No' ?>
					</a>
				</td>
			</tr>
			<?php $k = 1 - $k ?>
		<?php } ?>
		</table>
	</div>

	<?php echo Form::hidden('task', ''), "\n" ?>
	<?php echo Form::hidden('slug', ''), "\n" ?>
	<?php echo Form::hidden('sort', $sort), "\n" ?>
	<?php echo Form::hidden('order', $order), "\n" ?>
<?php echo Form::close(), "\n" ?>
