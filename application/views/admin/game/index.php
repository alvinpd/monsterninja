<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php echo $menu ?>

<script type="text/javascript">
	$(function(){
		$("#accordion-index-info").accordion({ header: "h3", autoHeight: false });
		$("#accordion-index-todo").accordion({ header: "h3" });
	})
</script>

<div class="span-6">
	<div class="span-6 last">
		<div id="accordion-index-info">
			<div>
				<h3><a href="#">Games</a></h3>
				<div>
					<p class="end"><?php echo number_format($total) ?> total games</p>
				</div>
			</div>
			<div>
				<h3><a href="#">Premium Games</a></h3>
				<div>
					<p><?php echo number_format($premium_count) ?> premium games</p>
					<p><?php echo number_format($premium_published) ?> published games</p>
					<p><?php echo number_format($premium_local) ?> hosted games</p>
					<p class="end"><?php echo number_format($premium_local_size/(1024 * 1024),2) ?> MB size of hosted games</p>
				</div>
			</div>
			<div>
				<h3><a href="#">Recommended Games</a></h3>
				<div>
					<p><?php echo number_format($recommended_count) ?> recommended games</p>
					<p class="end"><?php echo number_format($recommended_published) ?> published games</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="span-18 last">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-calculator"></div>
			Latest Premium Games
		</h3>
		<ul>
		<?php foreach($latest_premium as $game) : ?>
			<li>
				<?php
					echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game', 'action' => 'view', 'id' => $game->slug)),
								HTML::image($game->thumbnail_url,array('width'=>'60','height'=>'60')),
								array('title' => $game->name));
				?>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-calculator"></div>
			Latest Recommended Games
		</h3>
		<ul>
		<?php foreach($latest_recommended as $game) : ?>
			<li>
				<?php
					echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game', 'action' => 'view', 'id' => $game->slug)),
								HTML::image($game->thumbnail_url,array('width'=>'60','height'=>'60')),
								array('title' => $game->name));
				?>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-calculator"></div>
			Top Played Games
		</h3>
		<ul>
		<?php foreach($top_games as $game) : ?>
			<li>
				<?php
					echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game', 'action' => 'view', 'id' => $game['slug'])),
								HTML::image($game['thumbnail'],array('width'=>'60','height'=>'60')),
								array('title' => $game['name'].' ('.$game['plays'].') plays')), "\n"
				?>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-calculator"></div>
			Update History
		</h3>
		<div id="content-table">
			<table>
				<tr>
					<th width="30">#</th>
					<th>Generated</th>
					<th>Synchronized</th>
					<th width="80">Total</th>
					<th width="80">New</th>
					<th width="80">Updated</th>
				</tr>
			<?php $i = 1 ?>
			<?php $k = 0 ?>
			<?php foreach ($updates as $update) : ?>
				<tr class="row<?php echo $k ?>">
					<td><?php echo $i++ ?></td>
					<td><?php echo $update->generated ?></td>
					<td><?php echo $update->sync ?></td>
					<td><?php echo number_format($update->total) ?></td>
					<td><?php echo number_format($update->new) ?></td>
					<td><?php echo number_format($update->updated) ?></td>
				</tr>
				<?php $k = 1 - $k ?>
			<?php endforeach ?>
			</table>
		</div>
	</div>
</div>
