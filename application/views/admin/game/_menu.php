<?php defined('SYSPATH') or die('No direct script access.'); ?>

<?php $action = Request::instance()->action ?>
<div id="content-menu" class="span-24 last">
	<ul>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game')),
								 'Overview', array('class' => $action == 'index' ? 'selected' : '')) ?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game', 'action' => 'premium')),
								 'Premium Games', array('class' => $action == 'premium' ? 'selected' : ''))?>
		</li>
		<li>
			<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game', 'action' => 'recommended')),
								 'Recommended Games', array('class' => $action == 'recommended' ? 'selected' : '')) ?>
		</li>
	</ul>
</div>
