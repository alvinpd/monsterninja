<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php echo $menu ?>

<?php if ( $error ) : ?>
<div class="span-24">
	<div class="error">
		<?php echo $error ?>
	</div>
</div>
<?php else : ?>
<div class="span-4">
	<div class="content-info">
		<h3>
			<div class="image ui-icon-calculator"></div>
			<?php echo __('Thumbnail') ?>
		</h3>
		<p style="text-align: center"><?php echo HTML::image($game->thumbnail_url, array('title' => $game->name, 'alt' => $game->name)) ?></p>
	</div>
	<div class="content-info">
		<h3>
			<div class="image ui-icon-calculator"></div>
			<?php echo __('Information') ?>
		</h3>
		<p><?php echo $game->coins_enabled ? 'Premium' : 'Recommended ('.$game->recommendation.')' ?></p>
		<p><?php echo $game->category.' ('.$game->rating.')' ?></p>
		<p><?php echo HTML::anchor($game->author_link, $game->author, array('title' => 'Visit author profile.', 'target' => '_blank')) ?></p>
		<p><?php echo $game->width.'x'.$game->height ?></p>
	</div>
	<?php echo Form::open(null, array('name' => 'adminForm')), "\n" ?>
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				<?php echo __('Manage') ?>
			</h3>
		<?php if ($game->coins_enabled): ?>
			<p>
				<a href="javascript:void(0)" onclick="return doListItemTask('<?php echo $game->slug ?>', '<?php echo $game->local ? 'mochi' : 'server'?>')" title="Change swf file server location.">
					<?php echo $game->local ? 'Server' : 'Mochi'?>
				</a>
			</p>
		<?php endif ?>
			<p>
				<a href="javascript:void(0)" onclick="return doListItemTask('<?php echo $game->slug ?>', '<?php echo $game->published ? 'unpublish' : 'publish'?>')" title="Change publish settings.">
					<?php echo $game->published ? 'Published' : 'Unpublished'?>
				</a>
			</p>
			<p>
				<a href="javascript:void(0);" title="<?php echo $game->featured ? 'Unset featured' : 'Set featured'?>"
						 onclick="return doListItemTask('<?php echo $game->slug ?>','<?php echo $game->featured ? 'notfeatured' : 'featured'?>')">
						<?php echo $game->featured ? 'Featured' : 'Not Featured' ?>
				</a>
			</p>
			<p><?php echo number_format($game->swf_file_size / (1024 * 1024), 2) ?> MB</p>
		<?php if ($game->leaderboard_enabled) : ?>
			<p><?php echo number_format($plays).' '.__('Plays') ?></p>
		<?php else : ?>
			<p>No Leaderboard</p>
		<?php endif ?>
		</div>
		<?php echo Form::hidden('task', '') ?>
		<?php echo Form::hidden('slug', '') ?>
	<?php echo Form::close(), "\n" ?>

</div>

<div class="span-20 last">
	<div class="span-4">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				Date Created
			</h3>
			<p><?php echo HTML::date($game->created, '%b %d, %Y') ?></p>
		</div>
	</div>
	<div class="span-4">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				Last Update
			</h3>
			<p><?php echo HTML::date($game->updated, '%b %d, %Y') ?></p>
		</div>
	</div>
	<div class="span-4">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				Meta Score
			</h3>
			<p><?php echo number_format($game->metascore, 2) ?></p>
		</div>
	</div>
	<div class="span-4">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				Slug
			</h3>
			<p><?php echo $game->slug ?></p>
		</div>
	</div>
	<div class="span-4 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				Zip Archive
			</h3>
			<p>
				<a href="<?php echo $game->zip_url ?>" style="text-decoration: none" title="Download Zip">
					<?php echo HTML::image('media/images/zip.png', array('width' => '15', 'height' => '15')) ?>
					&nbsp;Download Here
				</a>
			</p>
		</div>
	</div>

	<div class="span-20 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				<?php echo $game->name ?>
			</h3>
			<p><?php echo $game->description ?>&nbsp;</p>
		</div>
	</div>

	<div class="span-20 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				<?php echo __('Instructions') ?>
			</h3>
			<p><?php echo $game->instructions ?>&nbsp;</p>
		</div>
	</div>

	<?php if (count($players) > 0) : ?>
	<div class="span-20 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				<?php echo __('Players') ?>
			</h3>
			<div style="height: 100%; margin: 0; border: 0; border: 0">
			<?php $i = 0 ?>
			<?php foreach ($players as $player) : ?>
				<div class="content-info1<?php echo ($i+1) % 4 == 0 ? ' end' : '' ?>">
					<a href="<?php echo URL::site(Route::get('admin')->uri(array('controller'=>'user','action'=>'view','id'=>$player['id']))) ?>"
						 title="User Name">
						<fb:profile-pic uid="<?php echo $player['username'] ?>" linked="false" size="square" width="50" height="50"></fb:profile-pic>
					</a>
					<h4><fb:name uid="<?php echo $player['username'] ?>" linked="false"></fb:name></h4>
					<?php echo number_format($player['plays']) ?>
				</div>
				<?php $i++ ?>
			<?php endforeach ?>
			</div>
		</div>
	</div>
	<?php endif ?>

	<div class="span-20 last">
		<div class="content-info">
			<h3>
				<div class="image ui-icon-calculator"></div>
				<?php echo __('Media') ?>
			</h3>
			<p>Images, screenshots and videos are placed here.</p>
		</div>
	</div>

</div>
<?php endif ?>
