<?php defined('SYSPATH') or die('No direct script access.'); ?>

<div class="main-menu">
  <ul>
    <li><?php echo HTML::anchor(Route::get('default')->uri(), 'home') ?></li>
    <li><?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'premium')), 'premium games') ?></li>
    <li><?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'recommended')), 'recommended games') ?></li>
	<?php if (Auth::instance()->logged_in()) : ?>
		<li><?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'bookmark')), 'bookmarked games') ?></li>
	<?php endif ?>
  </ul>
</div>
