<?php defined('SYSPATH') or die('No direct script access.');

  $facebook = Session::instance()->get('facebook');
  $fb_session = $facebook->getSession();

  $uid = null;
  if ( isset($fb_session) AND $fb_session != null ) {
    try {
      $uid = $facebook->getUser();
    } catch (FacebookApiException $e) {
      error_log($e);
    }
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='en' xml:lang='en' xmlns='http://www.w3.org/1999/xhtml' xmlns:fb='http://www.facebook.com/2008/fbml'>
	<head>
		<title><?php echo $title ?></title>

		<!-- Include required JavaScript files -->
<?php foreach ( $scripts as $script ) { ?>
		<?php echo HTML::script($script), "\n" ?>
<?php } ?>

		<!-- Include blueprintcss styles. -->
		<?php echo HTML::style('media/stylesheets/blueprint/screen.css', array('media' => 'screen, projection')), "\n" ?>
		<?php echo HTML::style('media/stylesheets/blueprint/print.css', array('media' => 'print')), "\n" ?>
		<!--[if lt IE 8]><?php echo HTML::style('media/stylesheets/blueprint/ie.css', array('media' => 'screen, projection')) ?><![endif]-->

		<!-- Include custom styles -->
		<?php echo HTML::style('media/stylesheets/mnhome.css', array('media' => 'screen, projection')), "\n" ?>

	</head>
	<body>
		<?php echo ORM::factory('setting')->where('name', '=', 'mochi_ver_code')->find()->value ?>

		<div id="fb-root"></div>
		<script type="text/javascript">
			window.fbAsyncInit = function() {
				FB.init({
					appId  : '<?php echo $facebook->getAppId() ?>',
					session: <?php echo json_encode($fb_session) ?>, // don't prefetch the session when PHP already has it
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true  // parse XFBML
				});
			};

			(function() {
				var e = document.createElement('script');
				e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
				e.async = true;
				document.getElementById('fb-root').appendChild(e);
			}());
		</script>

		<?php echo $content ?>

		<div id='site-notice'>
			<div class='container'>
				This website is under heavy development.&nbsp;
				Please report any bugs, comments and suggestions to the <a href="mailto:webmaster@localhost">webmaster</a>.
			</div>
		</div>

	</body>
</html>
