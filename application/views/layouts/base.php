<?php defined('SYSPATH') or die('No direct script access.');

	$action = Request::instance()->action;
	$controller = Request::instance()->controller;

	$facebook = Session::instance()->get('facebook');
	$fb_session = $facebook->getSession();

	$uid = null;
	if ( isset($fb_session) AND $fb_session != null ) {
		try {
			$uid = $facebook->getUser();
		} catch (FacebookApiException $e) {
			error_log($e);
		}
	}

	$logged_in = ($uid != null);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<title><?php echo $title ?></title>

		<meta name="description" content="Place the website's brief description here."/>
		<meta name="keywords" content="Place the website's keywords here."/>

		<!-- Include required JavaScript files -->
<?php foreach ( $scripts as $script ) { ?>
		<?php echo HTML::script($script), "\n" ?>
<?php } ?>

		<!-- Include blueprintcss styles. -->
		<?php echo HTML::style('media/stylesheets/blueprint/screen.css', array('media' => 'screen, projection')), "\n" ?>
		<?php echo HTML::style('media/stylesheets/blueprint/print.css', array('media' => 'print')), "\n" ?>
		<!--[if lt IE 8]><?php echo HTML::style('media/stylesheets/blueprint/ie.css', array('media' => 'screen, projection')) ?><![endif]-->

		<!-- Include custom home styles -->
		<?php echo HTML::style('media/stylesheets/mnhome.css', array('media' => 'screen, projection')), "\n" ?>

		<script type='text/javascript'>
			//<![CDATA[
				$(document).ready(function(){
				
					// Add some javascript code to the search input box
					$('#search-name').focus(function(event){
						if ( $.trim($(this).val()) == 'Search item...' ) {
							$(this).css({"font-style":"normal", color:"#000"}).val('');
						}
					});
					$('#search-name').focusout(function(event){
						if ( $.trim($(this).val()) == '' ) {
							$(this).css({"font-style":"italic", color:"#888"}).val('Search item...');
						}
					});
				});
			//]]>
		</script>
	</head>
	<body>
		<?php echo ORM::factory('setting')->where('name', '=', 'mochi_ver_code')->find()->value ?>

		<div id="fb-root"></div>
		<script type="text/javascript">
			window.fbAsyncInit = function() {
				FB.init({
					appId  : "<?php echo $facebook->getAppId() ?>",
					session: <?php echo json_encode($fb_session) ?>, // don't prefetch the session when PHP already has it
					status : true, // check login status
					cookie : true, // enable cookies to allow the server to access the session
					xfbml  : true  // parse XFBML
				});

				// whenever the user logs in, we refresh the page
        FB.Event.subscribe('auth.login', function() {
          window.location.reload();
        });
			};

			(function() {
				var e = document.createElement('script');
				e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
				e.async = true;
				document.getElementById('fb-root').appendChild(e);
			}());
		</script>

		<!-- Show the top div -->
		<div id='top'>
			<div class='container'>
				<div class='span-1'>
				<?php if ( $uid ) : ?>
					<fb:profile-pic uid="loggedinuser" linked="true" size="square" width="20" height="20"></fb:profile-pic>
				<?php else : ?>
					&nbsp;
				<?php endif ?>
				</div>
				<div class='span-15'>
				<?php if ( $uid ) : ?>
					<div class='top-menu'>
						<?php echo HTML::anchor(Route::get('user')->uri(array('controller' => 'profile')), 'Profile'), "\n" ?>
						<?php echo HTML::anchor(Route::get('user')->uri(array('controller' => 'profile', 'action' => 'bookmark')), 'Manage Bookmarks'), "\n" ?>
					</div>
				<?php else : ?>
					&nbsp;
				<?php endif ?>
				</div>
				<div class='span-8 last'>
					<div class='top-menu' style="float:right">
						<?php if ( !$uid ) : ?>
							<?php echo HTML::anchor(Route::get('user')->uri(array('action' => 'login')).URL::query(array('next' => Request::instance()->uri() ) ),
												 HTML::image('http://static.ak.fbcdn.net/rsrc.php/zB6N8/hash/4li2k73z.gif')), "\n" ?>
						<?php else : ?>
							<?php echo HTML::anchor(Route::get('user')->uri(array('action' => 'logout')), 'Logout'), "\n" ?>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>

		<!-- Show the header div -->
		<div id='header'>
			<div class='container' id='header-top'>
				&nbsp;
			</div>
			<div class='container' id='header-body'>
				<div class='span-12'>
					<h2 class="alt" style="color: #806600; margin-left: 20px;">Monster Ninja Games</h2>
				</div>
				<div class='span-12 last' id='header-menu'>
					<ul>
						<li>
							<?php echo HTML::anchor(Route::get('default')->uri(), 'home',
											array('title' => 'Go to the home page.', 'class' => ($controller == 'home' AND $action == 'index') ? 'selected' : '')), "\n" ?>
						</li>
						<li>
							<?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'premium')), 'premium games',
											array('title' => 'Go to the premium games page.', 'class' => ($controller == 'home' AND $action == 'premium') ? 'selected' : '')), "\n" ?>
						</li>
						<li<?php $logged_in ? '' : ' class="end"'?>>
							<?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'recommended')), 'recommended games',
											array('title' => 'Go to the recommended games page.', 'class' => ($controller == 'home' AND $action == 'recommended') ? 'selected' : '')), "\n" ?>
						</li>
					<?php if ($logged_in) : ?>
						<li class="end">
							<?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'bookmark')), 'bookmarked games',
											array('title' => 'Go to the bookmarked games page.', 'class' => ($controller == 'home' AND $action == 'bookmark') ? 'selected' : '')), "\n" ?>
						</li>
					<?php endif ?>
					</ul>
				</div>
			</div>
		</div>

		<!-- Show the content div -->
		<div id='content'>
			<div class='container' id='content-body'>
				<?php echo $content ?>
			</div>
		</div>

		<!-- Show the footer div -->
		<div id='footer'>
			<div class='container' id='footer-body'>
				<hr />
				<div class='span-10'>
					<p>
						Copyright &copy; 2010 Monster Ninja Games. All rights reserved.
					</p>
					<p>
						Trademarks mentioned in this site are owned by their respected owner.
					</p>
				</div>
				<div class='span-10'>
					<p>
						The games hosted in this web site are owned by their respected owner. The game
						developer's profile can be accessed through the link provided in the game play page.
					</p>
				</div>
				<div class='span-4 last'></div>
			</div>
			<div class='container' id='footer-bottom'>
				&nbsp
			</div>
		</div>

		<!-- Show the notice div -->
		<div id='site-notice'>
			<div class='container'>
				This website is under heavy development.&nbsp;
				Please report any bugs, comments and suggestions to the <a href="mailto:webmaster@monsterninja.x10hosting.com">webmaster</a>.
			</div>
		</div>

	</body>
</html>
