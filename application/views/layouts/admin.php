<?php defined('SYSPATH') or die('No direct script access.');

	$facebook = Session::instance()->get('facebook');
	$fb_session = $facebook->getSession();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"  xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<title><?php echo $title ?></title>

		<!-- Include JavaScript files -->
<?php foreach($scripts as $script) : ?>
		<?php echo HTML::script($script), "\n" ?>
<?php endforeach ?>

		<!-- Include blueprint css styles -->
		<?php echo HTML::style('media/stylesheets/blueprint/screen.css', array('media' => 'screen, projection')), "\n" ?>
		<?php echo HTML::style('media/stylesheets/blueprint/print.css', array('media' => 'print')), "\n" ?>
		<!--[if lt IE 8]><?php echo HTML::style('media/stylesheets/blueprint/ie.css', array('media' => 'screen, projection')) ?><![endif]-->

		<!-- Include jquery-ui styles -->
		<?php echo HTML::style('media/stylesheets/redmond/jquery-ui-1.8.1.custom.css', array('media' => 'screen, projection')), "\n"?>

		<!-- Include custom styles -->
		<?php echo HTML::style('media/stylesheets/mnadmin.css', array('media' => 'screen, projection')), "\n" ?>
	</head>

	<body>
		<div id="fb-root"></div>
		<script type="text/javascript">
			window.fbAsyncInit = function() {
				FB.init({
					appId  : '<?php echo $facebook->getAppId() ?>',
					<?php if ($fb_session) { ?>
					session: '<?php echo $fb_session ?>',
					<?php } ?>
					xfbml  : true  // parse XFBML
				});
			};

			(function() {
				var e = document.createElement('script');
				e.async = true;
				e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
				document.getElementById('fb-root').appendChild(e);
			}());
		</script>

		<div id="header">
			<div class="container">
				<div class="span-13">
					<div id="admin-menu">
						<?php $controller = Request::instance()->controller ?>
						<ul>
						<?php if (Auth::instance()->logged_in()) : ?>
							<li>
								<?php echo HTML::anchor(Route::get('admin')->uri(),
													 __('Main'), array('class' => $controller == 'main' ? 'selected' : '',
																					'title' => __('Go to the main admin page.'))), "\n" ?>
							</li>
							<li>
								<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'game')),
													 __('Games'), array('class' => $controller == 'game' ? 'selected' : '',
																					'title' => __('Go to the games management page.'))), "\n" ?>
							</li>
							<li>
								<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'user')),
													 __('Users'), array('class' => $controller == 'user' ? 'selected' : '',
																					'title' => __('Go to the users management page.'))), "\n" ?>
							</li>
							<li>
								<?php echo HTML::anchor(Route::get('admin')->uri(array('controller' => 'setting')),
													 __('Settings'), array('class' => $controller == 'setting' ? 'selected' : '',
																					'title' => __('Go to the settings management page.'))) ?>
							</li>
							<li class="end">
								<?php echo HTML::anchor(Route::get('admin')->uri(array('action' => 'logout')), __('Logout'),
													 array('title' => __('Log out from the administration pages.'))), "\n" ?>
							</li>
						<?php else : ?>
							<li class="end"><?php echo HTML::anchor(Route::get('default')->uri(), __('View Homepage'),
													array('title' => __('Go to and view the website homepage.'))), "\n" ?></li>
						<?php endif ?>
						</ul>
					</div>
				</div>
				<div class="span-5" style="text-align: right">
				<?php if (Auth::instance()->logged_in()) : ?>
					Logged in as <?php echo Auth::instance()->get_user()->username ?>.
				<?php else : ?>
					Please log in.
				<?php endif ?>
				</div>
				<div class="span-6 last" style="text-align: right">
					Website Administration - <?php echo HTML::date('now', '%b %d, %Y') ?>
				</div>
			</div>
		</div>
		<div id="content">
			<div class="container">
				<?php echo $content, "\n" ?>
			</div>
		</div>
		<div id="footer">
			<div class="container">
				<div class="span-8">
					Copyright &copy; 2010 Owner. All rights reserved.
				</div>
				<div class="span-8" style="text-align: center">
					mem: {memory_usage}, time: {execution_time}
				</div>
				<div class="span-8 last" style="text-align: right">
					Powered by Kohana PHP <?php echo Kohana::VERSION ?>.
				</div>
			</div>
		</div>
	</body>
</html>
