<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!-- Display all the items on the main span -->
<div class='span-18'>
	<!-- Display the featured recommended items -->
	<div class="span-18 last">
		<div class="content-feature-top"></div>
		<div class="content-feature">
			<h4>
				featured recommended games
			</h4>
		<?php $i = 1 ?>
		<?php foreach ( $featured as $game ) : ?>
			<div class="content-feature-item<?php echo ($i == 1) ? ' start' : ''?><?php echo ($i == 6) ? ' end' : ''?>">
				<a href='<?php echo URL::site(Route::get('default')->uri(array('action' => 'play', 'id' => $game->slug))) ?>' title='Play <?php echo $game->name ?>'>
					<img alt='<?php echo $game->name ?>' height='80' src='<?php echo $game->thumbnail_url ?>' width='80' />
				</a>
				<?php echo $game->name, "\n" ?>
				<?php $i++ ?>
			</div>
		<?php endforeach ?>
		</div>
		<div class='content-feature-bottom'></div>
	</div>

	<div class="span-18 last" style="margin-bottom: 5px">
		<!-- Home page ad -->
		<!-- Begin: AdBrite, Generated: 2010-06-22 9:05:12  -->
		<script type="text/javascript">
			var AdBrite_Title_Color = 'E1771E';
			var AdBrite_Text_Color = '000000';
			var AdBrite_Background_Color = 'FFFF66';
			var AdBrite_Border_Color = 'C94093';
			var AdBrite_URL_Color = 'FF6FCF';
			try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}
		</script>
		<div style="text-align: center">
			<span style="white-space:nowrap;"><script type="text/javascript">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src="http://ads.adbrite.com/mb/text_group.php?sid=1672368&zs=3436385f3630&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'" type="text/javascript">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>
			<a target="_top" href="http://www.adbrite.com/mb/commerce/purchase_form.php?opid=1672368&afsid=1"><img src="http://files.adbrite.com/mb/images/adbrite-your-ad-here-banner.gif" style="background-color:#C94093;border:none;padding:0;margin:0;" alt="Your Ad Here" width="11" height="60" border="0" /></a></span>
		</div>
		<!-- End: AdBrite -->
	</div>

  <div class='span-18 last'>
    <div class='content-feature-top'></div>
    <div class="content-list">
      <h4>
        premium games
      </h4>
      <ul>
      <?php foreach ( $recommended as $game ) { ?>
        <li>
          <h4><?php echo $game->category ?></h4>
          <a href='<?php echo URL::site(Route::get('default')->uri(array('action' => 'play', 'id' => $game->slug))) ?>' title='Play <?php echo $game->name ?>'>
            <img height='60' src='<?php echo $game->thumbnail_url ?>' width='60' alt='<?php echo $game->name ?>'/>
          </a>
          <span>
            <a href='<?php echo URL::site(Route::get('default')->uri(array('action' => 'play', 'id' => $game->slug))) ?>' title='Play <?php echo $game->name ?>'>
              <?php echo $game->name ?>
            </a>
          </span>
          <p>
            <?php echo $game->description ?>
          </p>
        </li>
      <?php } ?>
      </ul>
      <br />
    </div>
    <div class='content-feature-bottom'></div>
  </div>
</div>

<!-- Display all the items on the right span -->
<div class='span-6 last'>
	<?php
	/*
  <div class='span-6 last' id='search'>
    <input id='search-name' size='24' type='text' value='Search item...' />
    <input class='button' type='submit' value='Search' />
  </div>
	*/ ?>

  <div class='span-6 last' id='link'>
    <a href='https://www.mochimedia.com/r/7561952edbaa272e' title='Powered by Mochiads'>
      <img src='/media/images/referral.png' alt='Powered by Mochiads' />
    </a>
  </div>
  <div class='span-6 last'>
    <div class='content-right-top'></div>
    <div class='content-right'>
      <h4>
        top games
      </h4>
		<?php foreach ($top_games as $game) : ?>
			<div class="content-right-item">
				<a href="<?php echo URL::site(Route::get('default')->uri(array('action' => 'play','id' => $game['slug']))) ?>"
					 title="Play <?php echo $game['name'], "\n" ?>">
					<?php echo HTML::image($game['thumbnail'], array('width' => '40', 'height' => '40')), "\n" ?>
					<?php echo $game['name'], "\n" ?>
				</a>
				<p style="margin: 0; padding: 0">
					<?php echo '('.$game['plays'].') plays', "\n" ?>
				</p>
			</div>
		<?php endforeach ?>
    </div>
    <div class='content-right-bottom'></div>
  </div>
  <div class='span-6 last'>
    <div class='content-right-top'></div>
    <div class='content-right'>
      <h4>
        top players
      </h4>
		<?php foreach($top_players as $player) : ?>
			<div class="content-right-item">
				<div class="content-user-image" style="margin: 0; padding: 0">
					<fb:profile-pic uid="<?php echo $player['userID'] ?>" linked="true" size="square" width="40" height="40"/>
				</div>
				<div class="content-user-name" style="margin: 0; padding: 0">
					<fb:name uid="<?php echo $player['userID'] ?>"/>
				</div>
				<p style="margin: 0; padding: 0">
					<?php echo ' ('.$player['plays'].') plays' ?>
				</p>
			</div>
		<?php endforeach ?>
    </div>
    <div class='content-right-bottom'></div>
  </div>

	<!-- List the latest recommended games -->
	<div class='span-6 last'>
		<div class='content-right-top'></div>
		<div class='content-right'>
			<h4>
				latest games
			</h4>
		<?php foreach($latest_games as $game) :?>
			<div class="content-right-item">
				<a href="<?php echo URL::site(Route::get('default')->uri(array('action' => 'play','id' => $game->slug))) ?>" title="Play <?php echo $game->name ?>">
					<?php echo HTML::image($game->thumbnail_url, array('width'=>'40','height'=>'40')), "\n" ?>
					<?php echo $game->name, "\n" ?>
				</a>
				<p style="margin: 0; padding: 0">
					<?php echo HTML::date($game->updated, '%b, %d %Y'), "\n" ?>
				</p>
			</div>
		<?php endforeach ?>
		</div>
		<div class='content-right-bottom'></div>
	</div>
</div>

<div class="span-24 last">
	<!-- Place the ad here -->
	<!-- Begin: AdBrite, Generated: 2010-06-20 5:20:15  -->
	<script type="text/javascript">
		var AdBrite_Title_Color = 'E1771E';
		var AdBrite_Text_Color = '000000';
		var AdBrite_Background_Color = 'FFFF66';
		var AdBrite_Border_Color = 'C94093';
		var AdBrite_URL_Color = 'C94093';
		try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}
	</script>
	<div style="text-align: center">
		<span style="white-space:nowrap;"><script type="text/javascript">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src="http://ads.adbrite.com/mb/text_group.php?sid=1670099&zs=3732385f3930&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'" type="text/javascript">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>
		<a target="_top" href="http://www.adbrite.com/mb/commerce/purchase_form.php?opid=1670099&afsid=1"><img src="http://files.adbrite.com/mb/images/adbrite-your-ad-here-leaderboard.gif" style="background-color:#C94093;border:none;padding:0;margin:0;" alt="Your Ad Here" width="14" height="90" border="0" /></a></span>
	</div>
<!-- End: AdBrite -->
</div>
