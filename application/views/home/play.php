<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php if ($uid) : ?>
<script type="text/javascript">
	function bookmarkGame(slug)
	{
		alert(slug);
	}
</script>

<?php endif ?>
<!-- Display the top div -->
<div id='top'>
	<div class='container'>
		<div class='span-16'>
			<div id='top-details'>
				<ul>
					<li>
						<?php echo HTML::anchor($back, 'Back', array('title' => __('Go back to the previous page'))), "\n" ?>
					</li>
					<li>
						<h4 title="Game Name"><?php echo $game->name ?></h4>
					</li>
					<li>
						<h4 title="Game Category"><?php echo $game->category ?></h4>
					</li>
					<li>
						<img alt='Recommendation <?php echo $game->recommendation ?>' src='/media/images/star-<?php echo $game->recommendation ?>.png' title="Recommendation" />
					</li>
				<?php if ($uid) : ?>
					<li>
						<a href="javascript:void(0);" title="Bookmark this game." onclick="return bookmarkGame('<?php echo $game->slug ?>');">Bookmark Game</a>
					</li>
				<?php endif ?>
				</ul>
			</div>
		</div>
		<div class='span-8 last' style="text-align: right">
			<div class='top-menu'>
				<?php if ( !$uid ) : ?>
					<?php echo HTML::anchor(Route::get('user')->uri(array('action' => 'login')).URL::query(array('next' => Request::instance()->uri())),
										 HTML::image('http://static.ak.fbcdn.net/rsrc.php/zB6N8/hash/4li2k73z.gif')) ?>
				<?php else :?>
					<?php echo HTML::anchor(Route::get('user')->uri(array('action' => 'logout')), 'Logout') ?>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>

<?php if (!$uid AND $game->leaderboard_enabled) : ?>
<div class="container">
	<div class="span-24 last">
		<div class="notice">
			Please log in using your Facebook account to save the score you can achieve.
			More importantly, enjoy the game.
		</div>
	</div>
</div>
<?php endif ?>

<?php if ($achievement AND $achievement->loaded()) : ?>
<div id="header">
	<div class="container" id="header-top">&nbsp;</div>
	<div class="container" id="header-body">
		<div class="span-24">
			<div id="game-achievement">
				<h3>game achievements</h3>
				<div class="game-achievement-detail">
					<?php echo HTML::image($game->thumbnail_url) ?>
					<h4>Plays</h4>
					<?php echo number_format($achievement->plays) ?>
				</div>
				<div class="game-achievement-detail">
					<h4><?php echo $leaderboard->title ?></h4>
					<?php echo number_format($achievement->score) ?>
				</div>
				<div class="game-achievement-detail">
					<?php echo HTML::image('http://cdn.mochiads.com/c/achievement/score_gold.jpg') ?>
					<h4>Gold Medals</h4>
					<?php echo number_format($achievement->medal_gold) ?>
				</div>
				<div class="game-achievement-detail">
					<?php echo HTML::image('http://cdn.mochiads.com/c/achievement/score_silver.jpg') ?>
					<h4>Silver Medals</h4>
					<?php echo number_format($achievement->medal_silver) ?>
				</div>
				<div class="game-achievement-detail">
					<?php echo HTML::image('http://cdn.mochiads.com/c/achievement/score_bronze.jpg') ?>
					<h4>Bronze Medals</h4>
					<?php echo number_format($achievement->medal_bronze) ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="footer" style="margin-bottom: 0px">
	<div class='container' id='footer-bottom' style="padding-bottom: 15px">
		&nbsp
	</div>
</div>
<?php endif ?>

<div id='play'>
  <div class='container' id='play-info'>
    <div class='span-3'>
      <img src='<?php echo $game->thumbnail_url ?>' alt='<?php echo $game->name ?>' />
    </div>
    <div class='span-16'>
      <h4>
        Description
      </h4>
      <p>
        <?php echo $game->description ?>
      </p>
    </div>
		<div class="span-5 last">
			<h4>
				Game Author
			</h4>
			<p>
				<?php echo HTML::anchor($game->author_link, $game->author, array('title' => 'Visit the authors profile here.')) ?>
			</p>
		</div>
  </div>

<?php
  if ( $game->local ) {
    $swf = '/media/swf/'.$game->slug.'.swf';
  } else {
    $swf = $game->swf_url;
  }
?>
  <div id="play-game" style="width: <?php echo $game->width ?>; height: <?php echo $game->height ?>; margin: 20px auto 50px auto; text-align: center">
    <object width="<?php echo $game->width ?>" height="<?php echo $game->height ?>">
      <param name="<?php echo $game->slug ?>" value="<?php echo $swf ?>">
      <embed src="<?php echo $swf ?>" width="<?php echo $game->width ?>" height="<?php echo $game->height ?>">
      </embed>
    </object>
  </div>
</div>

<?php if ( $game->leaderboard_enabled ) { ?>

<div class="container" style="margin-bottom: 50px">
  <div class="span-24 last" style="text-align: center">
    <div id="leaderboard_bridge"></div>
    <div id="leaderboard_widget"></div>
  </div>
</div>

<?php } else { ?>
<div id="leaderboard_bridge"></div>
<?php } ?>

<script type="text/javascript">
  //<![CDATA[
  // Mochi Bridge
  var options = {
    partnerID: "<?php echo ORM::factory('setting')->where('name', '=', 'mochi_pub_id')->find()->value ?>",
    id: "leaderboard_bridge"
  };

  // https://www.mochimedia.com/support/pub_docs#boards
  //options.logoURL = "";
  options.siteURL = "<?php echo URL::base(false, true) ?>";
  options.siteName = "Monster Ninja Games";

<?php if ( $uid ) :?>
  // Optional Items
  options.userID = "<?php echo $uid ?>";
  options.profileURL = "http://www.facebook.com/profile.php?id=<?php echo $uid ?>";
  options.thumbURL = "http://graph.facebook.com/<?php echo $uid ?>/picture"
  options.sessionID = "<?php echo $session['session_key'] ?>"
  options.postMedals = true;

  options.callback = function(params) {
    //alert("gameID: "+params.gameID+
    //      "\nboardID: "+params.boardID+
    //      "\ntitle: "+params.title+
    //      "\ndescription: "+params.description+
    //      "\ndatatype: "+params.datatype+
    //      "\nsortOrder: "+params.sortOrder+
    //      "\nscoreLabel: "+params.scoreLabel+
    //      "\n\nsubmission: "+params.submission+
    //      "\nuserID: "+params.userID+
    //      "\nname: "+params.name+
    //      "\nusername: "+params.username+
    //      "\nsessionID: "+params.sessionID+
    //      "\nscore: "+params.score+
    //      "\nthumbnail: "+params.thumbnail+
    //      "\n\nsignature: "+params.signature
    //);

    var postData = "";
    if ( params.boardID && params.boardID != "null" ) {
      postData += "boardID="+params.boardID+"&";
    }

    if ( params.datatype && params.datatype != "null" ) {
      postData += "datatype="+params.datatype+"&";
    }

    if ( params.description && params.description != "null" ) {
      postData += "description="+escape(params.description)+"&";
    }

    if ( params.gameID && params.gameID != "null" ) {
      postData += "gameID="+params.gameID+"&";
    }

    if ( params.name && params.name != "null" ) {
      postData += "name="+escape(params.name)+"&";
    }

    if ( params.score && params.score != "null" ) {
      postData += "score="+params.score+"&";
    }

    if ( params.scoreLabel && params.scoreLabel != "null" ) {
      postData += "scoreLabel="+params.scoreLabel+"&";
    }

    if ( params.sessionID && params.sessionID != "null" ) {
      postData += "sessionID="+params.sessionID+"&";
    }

    if ( params.signature && params.signature != "null" ) {
      postData += "signature="+params.signature+"&";
    }

    if ( params.sortOrder && params.sortOrder != "null" ) {
      postData += "sortOrder="+params.sortOrder+"&";
    }

    if ( params.submission && params.submission != "null" ) {
      postData += "submission="+params.submission+"&";
    }

    if ( params.thumbnail && params.thumbnail != "null" ) {
      postData += "thumbnail="+params.thumbnail+"&";
    }

    if ( params.title && params.title != "null" ) {
      postData += "title="+escape(params.title)+"&";
    }

    if ( params.userID ) {
      postData += "userID="+params.userID;
    }

    //alert(postData);

    // Submit score to the site
    $.ajax({
      type: "POST",
      url: "<?php echo URL::site( Route::get('ajax')->uri() ) ?>",
      data: postData
    });
  }
<?php endif ?>

  Mochi.addLeaderboardIntegration(options);

<?php if ( $game->leaderboard_enabled ) { ?>
  options.game = "<?php echo $game->slug ?>";
  options.width = 500;
  options.height = 300;
  options.defaultTab = "alltime";
  options.backgroundColor = "#FFD42A";

  options.id = "leaderboard_widget";
  Mochi.showLeaderboardWidget(options);
<?php } ?>

  //]]>
</script>

<div class="container">
	<div class="span-24">
		<!-- Place the ad here -->
		<!-- Begin: AdBrite, Generated: 2010-06-20 5:20:15  -->
		<script type="text/javascript">
			var AdBrite_Title_Color = 'E1771E';
			var AdBrite_Text_Color = '000000';
			var AdBrite_Background_Color = 'FFFF66';
			var AdBrite_Border_Color = 'C94093';
			var AdBrite_URL_Color = 'C94093';
			try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}
		</script>
		<div style="text-align: center">
			<span style="white-space:nowrap;"><script type="text/javascript">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src="http://ads.adbrite.com/mb/text_group.php?sid=1670099&zs=3732385f3930&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'" type="text/javascript">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>
			<a target="_top" href="http://www.adbrite.com/mb/commerce/purchase_form.php?opid=1670099&afsid=1"><img src="http://files.adbrite.com/mb/images/adbrite-your-ad-here-leaderboard.gif" style="background-color:#C94093;border:none;padding:0;margin:0;" alt="Your Ad Here" width="14" height="90" border="0" /></a></span>
		</div>
	<!-- End: AdBrite -->
	</div>
</div>
