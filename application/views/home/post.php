<?php defined('SYSPATH') or die('No direct script access.'); ?>

<script type="text/javascript">
	function submit_score()
	{
		var postData =	"boardID=e9173da614b3058ba692646fe81de62b"+
										"&datatype=number"+
										"&gameID=03725f1c28015462"+
										"&name=Alvin%20Difuntorum"+
										"&score=60"+
										"&scoreLabel=Score"+
										"&sessionID=thisisthesessionidvalue"+
										"&sortOrder=desc"+
										"&submission=score"+
										"&title=Highscores"+
										"&userID=1231543040";

		alert('Posting score. '+postData);
		$.ajax({
			type: "POST",
			url: "<?php echo URL::site( Route::get('ajax')->uri() ) ?>",
			data: postData
		});
	}

	function submit_medal()
	{
		var postData =	"description="+escape("Excellent! Gold Medal Score!")+
										"&gameID=03725f1c28015462"+
										"&name="+escape("Gold Medal")+
										"&sessionID=thisisthesessionidvalue"+
										"&sortOrder=desc"+
										"&submission=medal"+
										"&thumbnail=http://cdn.mochiads.com/c/achievements/score_gold.jpg"+
										"&userID=1231543040";

		alert('Posting medal. '+postData);
		$.ajax({
			type: "POST",
			url: "<?php echo URL::site( Route::get('ajax')->uri() ) ?>",
			data: postData
		});
	}
</script>

<a href="javascript:void(0)" onclick="return submit_score()">Post Score</a><br/>
<a href="javascript:void(0)" onclick="return submit_medal()">Post Medal</a>
