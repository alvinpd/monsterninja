<?php defined('SYSPATH') or die('No direct script access.'); ?>

<!-- Display the user contents -->
<div id ="user" class="span-18">
	<div class="user-info">
		<div class="user-info-image">
			<fb:profile-pic uid="loggedinuser" linked ="false" facebook-logo="true" size="square"></fb:profile-pic>
		</div>
		<div class="user-info-name">
			<fb:name uid="loggedinuser" useyou="false"></fb:name>
		</div>
		<p>Welcome to your profile.</p>
	</div>

	<div class="span-18 last">
		<div class="user-medal">
			<div class="user-medal-info">
				<?php echo HTML::image('http://cdn.mochiads.com/c/achievement/score_gold.jpg'), "\n" ?>
				<h4><?php echo __('Gold Medals') ?></h4>
				<p><?php echo number_format($gold) ?></p>
			</div>
			<div class="user-medal-info">
				<?php echo HTML::image('http://cdn.mochiads.com/c/achievement/score_silver.jpg'), "\n" ?>
				<h4><?php echo __('Silver Medals') ?></h4>
				<p><?php echo number_format($silver) ?></p>
			</div>
			<div class="user-medal-info">
				<?php echo HTML::image('http://cdn.mochiads.com/c/achievement/score_bronze.jpg'), "\n" ?>
				<h4><?php echo __('Bronze Medals') ?></h4>
				<p><?php echo number_format($bronze) ?></p>
			</div>
		</div>
	</div>

  <div class="span-18 last">
		<div class='content-feature-top'></div>
		<div class="content-feature">
			<h4>
				latest friend activities
			</h4>
		<?php foreach($activities as $activity) : ?>
			<div class="content-activity">
				<h2><?php echo $activity['title']?></h2>
				<div class="content-activity-image">
					<fb:profile-pic uid="<?php echo $activity['uid'] ?>" linked="true" size="square" width="40" height="40"/>
				</div>
				<h3>
					<?php echo HTML::anchor(Route::get('default')->uri(array('action' => 'play','id' => $activity['slug'])),
																	$activity['name'], array('title' => 'Play '.$activity['name'])) ?>
				</h3>
				<p><?php echo number_format($activity['score']) ?></p>
			</div>
		<?php endforeach ?>
		</div>
		<div class='content-feature-bottom'></div>
  </div>
</div>

<!-- Display all the items on the right span -->
<div class='span-6 last'>
  <div class='span-6 last' id='search'>
    <input id='search-name' size='24' type='text' value='Search item...' />
    <input class='button' type='submit' value='Search' />
  </div>
  <div class='span-6 last' id='link'>
    <a href='https://www.mochimedia.com/r/7561952edbaa272e' title='Powered by Mochiads'>
      <img src='/media/images/referral.png' alt='Powered by Mochiads' />
    </a>
  </div>
  <div class='span-6 last'>
    <div class='content-right-top'></div>
    <div class='content-right'>
      <h4>
        top games
      </h4>
<?php foreach ($top_games as $game) : ?>
			<div class="content-right-item">
				<a href="<?php echo URL::site(Route::get('default')->uri(array('action' => 'play','id' => $game['slug']))) ?>"
					 title="Play <?php echo $game['name'] ?>">
					<?php echo HTML::image($game['thumbnail'], array('width' => '40', 'height' => '40')), "\n" ?>
					<?php echo $game['name'], "\n" ?>
				</a>
				<p style="margin: 0; padding: 0">
					<?php echo '('.$game['plays'].') plays', "\n" ?>
				</p>
			</div>
<?php endforeach ?>
    </div>
    <div class='content-right-bottom'></div>
  </div>
</div>
